import * as React from "react";
import {SyntheticEvent} from "react";
import {ModalComponentProps, ModalComponentState} from "../../Common/modal-component";
import autobind from "autobind-decorator";
import {Button, FormControl, Modal} from "react-bootstrap";
import ACTIONS from "../flux/actions";

export default class ApplicationStateInputModal extends React.PureComponent<ModalComponentProps, ModalComponentState> {
    protected inputControl: HTMLInputElement;

    constructor (props: ModalComponentProps) {
        super(props);

        this.state = {
            open: true
        };
    }

    fillAndShow (): void {
        this.show();
    }

    protected onSubmit () {
        this.props.dispatch(ACTIONS.CREATE_SET_STATE(JSON.parse(this.inputControl.value)));
    }

    @autobind
    handleOk (e?: SyntheticEvent<any>) {
        !e || e.preventDefault();
        this.onSubmit();
        this.hide();
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    show () {
        this.setState({
            open: true
        });
    }

    @autobind
    focusSelectInput () {
        this.inputControl.focus();
        this.inputControl.select();
    }


    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    render () {
        return (
            <Modal
                show={this.state.open}
                onEntered={this.focusSelectInput}
                onExited={this.removeModal}
                onHide={this.hide}
                backdrop
            >
                <form onSubmit={this.handleOk}>
                    <Modal.Header closeButton>
                        <Modal.Title>Enter application state in JSON format</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormControl
                            inputRef={(input) => this.inputControl = input as HTMLInputElement}
                            componentClass="textarea"
                        />
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                        >
                            Apply
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}
