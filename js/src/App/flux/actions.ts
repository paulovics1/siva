import MainState from "./main-state";
import Problem from "../../Problem/problem";

export interface AnyAction {
    type: string
}

export namespace ACTIONS {
    export const CREATE_WORKSPACE = "CREATE_WORKSPACE";
    export const SET_STATE = "SET_STATE";
    export const RESET_STATE = "RESET_STATE";
    export const CREATE_PROBLEM = "CREATE_PROBLEM";

    export function CREATE_SET_STATE (state: MainState): PAYLOAD_SET_STATE {
        return {
            type: SET_STATE,
            state: state
        };
    }

    export function CREATE_RESET_STATE (): PAYLOAD_RESET_STATE {
        return {
            type: RESET_STATE
        };
    }

    export function CREATE_CREATE_WORKSPACE (workspaceType: string, data?: any): PAYLOAD_CREATE_WORKSPACE {
        return {
            type: CREATE_WORKSPACE,
            workspaceType: workspaceType,
            data: data
        };
    }

    export function CREATE_CREATE_PROBLEM (problem: Problem): PAYLOAD_CREATE_PROBLEM {
        return {
            type: CREATE_PROBLEM,
            problem
        };
    }

    export interface PAYLOAD_SET_STATE extends AnyAction {
        state: MainState
    }

    export interface PAYLOAD_RESET_STATE extends AnyAction {

    }

    export interface PAYLOAD_CREATE_WORKSPACE extends AnyAction {
        workspaceType: string,
        data?: any
    }

    export interface PAYLOAD_CREATE_PROBLEM extends AnyAction {
        problem: Problem
    }
}

export default ACTIONS;