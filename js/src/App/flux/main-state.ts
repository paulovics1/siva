import Workspace from "../../Workspace/workspace";
import {initNormalizedCategoryInState, NormalizedStateIdCategory} from "../../Common/utils/util";
import Problem from "../../Problem/problem";

export interface GlobalStateObject {
    activeWorkspaceIds: number[]
}

export default interface MainState {
    GLOBAL: GlobalStateObject
    Workspaces: NormalizedStateIdCategory<Workspace>
    Problems: NormalizedStateIdCategory<Problem>
}

export function createNewEmptyMainState () {
    let state = {} as MainState;
    state.GLOBAL = createNewEmptyGlobalStateObject();
    state = initNormalizedCategoryInState(state, "Workspaces");
    state = initNormalizedCategoryInState(state, "Problems");
    return state;
}

export function createNewEmptyGlobalStateObject () {
    return {
        activeWorkspaceIds: []
    };
}