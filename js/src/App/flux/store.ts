import {createStore} from "redux";
import WorkspaceRegister from "../../Workspace/utils/workspace-register";
import {default as MainState} from "./main-state";
import {chainReducers} from "../../Common/utils/util";
import MainReducer from "./main-reducer";

export function loadState () {
    try {
        const serializedState = localStorage.getItem("state");
        if (serializedState) {
            return JSON.parse(serializedState);
        }
    }
    catch (e) {}
    return undefined;
}

export function saveState (state: MainState) {
    try {
        localStorage.setItem("state", JSON.stringify(state));
    }
    catch (e) {}
}

const store = createStore(
    chainReducers(
        ...WorkspaceRegister.getAll().map(w => w.reducer), MainReducer
    ),
    loadState()
);

export default store;

store.subscribe(() => {
    saveState(store.getState());
});
