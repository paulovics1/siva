import * as React from "react";
import {Button, Modal} from "react-bootstrap";
import autobind from "autobind-decorator";
import {ModalComponentProps, ModalComponentState} from "../modal-component";

export interface ConfirmationModalProps extends ModalComponentProps {
    title: string
    message: string
    onConfirm: () => void
}

export interface ConfirmationModalState extends ModalComponentState {

}

export default class ConfirmationModal extends React.PureComponent<ConfirmationModalProps, ConfirmationModalState> {
    constructor (props: ConfirmationModalProps) {
        super(props);

        this.state = {
            open: true
        };
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    @autobind
    handleProceed () {
        this.props.onConfirm();
        this.hide();
    }

    render () {
        return (
            <Modal
                show={this.state.open}
                onHide={this.hide}
                onExited={this.removeModal}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    {this.props.message}
                </Modal.Body>
                <Modal.Footer>
                    <Button
                        onClick={this.hide}
                    >
                        Cancel
                    </Button>
                    <Button
                        bsStyle="primary"
                        onClick={this.handleProceed}
                    >
                        Confirm
                    </Button>
                </Modal.Footer>
            </Modal>
        );
    }
}