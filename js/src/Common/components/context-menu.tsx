import * as React from "react";
import {MouseEvent, ReactElement, SyntheticEvent} from "react";
import {ListGroup, ListGroupItem} from "react-bootstrap";
import autobind from "autobind-decorator";

export interface ContextMenuProps {
    data: ContextMenuData,
    hide: () => void,
}

export default class ContextMenu extends React.PureComponent<ContextMenuProps> {
    @autobind
    handleCoverClick (e: SyntheticEvent<HTMLDivElement>) {
        e.preventDefault();
        this.props.hide();
    }

    static disabledClickHandler (e: MouseEvent<ListGroupItem>) {
        e.stopPropagation();
    }

    render () {
        return (
            <div className="context-menu-wrapper"
                 onClick={this.handleCoverClick}
                 onContextMenu={this.handleCoverClick}
            >
                <div
                    className="context-menu-cover"
                    onClick={this.handleCoverClick}
                    onContextMenu={this.handleCoverClick}
                />
                <ListGroup
                    className="context-menu"
                    style={{
                        left: this.props.data.x,
                        top: this.props.data.y
                    }}
                >
                    {
                        this.props.data.contextMenuRowDataList.map((row, index) => {
                            return (<ListGroupItem
                                key={index}
                                onClick={row.disabled ? ContextMenu.disabledClickHandler : row.action}
                                disabled={typeof row.disabled === "undefined" ? false : row.disabled}
                            >
                                {row.title}
                            </ListGroupItem>);
                        })
                    }
                </ListGroup>
            </div>
        );
    }
}

export interface ContextMenuRowData {
    title: string | ReactElement<any>,
    action: () => void,
    disabled?: boolean
}

export interface ContextMenuData {
    x: number,
    y: number,
    contextMenuRowDataList: ContextMenuRowData[]
}