import {default as Workspace} from "../../../../Workspace/workspace";
import {appendNormalizedToState, setInState} from "../../../../Common/utils/util";
import {createNewEmptyKnowledgeBase, default as KnowledgeBase} from "../../../Denotation/KnowledgeBase/knowledge-base";
import MainState from "../../../../App/flux/main-state";
import {TableauNodeState} from "../../../../Tableau/tableau-node";
import {TableauState} from "../../../../Tableau/tableau";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import CTREE_HELPER_FUNCTIONS from "../utils/c-tree-helper-functions";
import {CTreeState, CTreeTableauType} from "../c-tree";

export const DL_CTREE_TABLEAU_WORKSPACE_TYPE = "DLCTreeTableauWorkspace";

export default interface DLCTreeTableauWorkspace extends Workspace {
    knowledgeBaseId: number,
    cTreeId: number
}

export function createNewEmptyDLCTreeTableauWorkspace (state: DLCTreeTableauWorkspaceState) {
    let newKnowledgeBase;
    [state, newKnowledgeBase] = createNewEmptyKnowledgeBase(state) as [DLCTreeTableauWorkspaceState, KnowledgeBase];

    const result: DLCTreeTableauWorkspace = {
        type: DL_CTREE_TABLEAU_WORKSPACE_TYPE,
        knowledgeBaseId: newKnowledgeBase.id,
        cTreeId: NaN
    } as DLCTreeTableauWorkspace;

    state = appendNormalizedToState(state, ["Workspaces"], result);

    return [state, result] as [DLCTreeTableauWorkspaceState, DLCTreeTableauWorkspace];
}

export function updateTableauCurrentNodeState<S extends TableauState> (tableauId: number, state: S & DLCTreeTableauWorkspaceState): S {
    const tableau = state.Tableaus.byId[tableauId];
    const cTree = CTREE_HELPER_FUNCTIONS.getTableauCTree(tableauId, state.DLCTrees)!;
    const contradictions = CTREE_HELPER_FUNCTIONS.getCTreeContradictions(cTree, state.DLExpressions, state.DLCTreeNodes);

    if (Object.keys(contradictions).length !== 0) {
        state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTree.id, `One of the completion tree nodes contains a clash (${CTREE_HELPER_FUNCTIONS.getNodeName(state.DLCTreeNodes.byId[Number.parseInt(Object.keys(contradictions)[0])].nameOrIndividualSymbolId, state.DLVocabularySymbols)}).`, "negative");
        return setInState(state, ["TableauNodes", "byId", tableau.activeNodeId, "state"], TableauNodeState.FAILURE, true);
    }

    const blockedNodes = CTREE_HELPER_FUNCTIONS.getCTreeBlockedNodes(cTree, state.DLExpressions, state.DLCTreeNodes, state.DLCTreeEdges);

    for (const nodeId of cTree.nodes) {
        if (typeof blockedNodes[nodeId] === "undefined") {
            if (CTREE_HELPER_FUNCTIONS.getNewApplicableCTreeTableauConceptRuleNodes(nodeId, state.DLCTreeNodes, state.DLCTreeEdges, state.DLExpressions).length > 0 ||
                CTREE_HELPER_FUNCTIONS.getNewApplicableTableauTRuleNodes(nodeId, state.DLKnowledgeBases.byId[cTree.knowledgeBaseId].tBoxAxiomIds, state.DLCTreeNodes, state.DLCTreeEdges, state.DLExpressions).length > 0) {
                return setInState(state, ["TableauNodes", "byId", tableau.activeNodeId, "state"], TableauNodeState.NEUTRAL, true);
            }
        }
    }

    state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTree.id, `You have successfully created a fully expanded clash-free completion tree.`, "positive");
    return setInState(state, ["TableauNodes", "byId", tableau.activeNodeId, "state"], TableauNodeState.SUCCESS, true);
}

TableauRuleRouter.registerTableauCurrentNodeStateUpdateHandler(CTreeTableauType, updateTableauCurrentNodeState);

export interface DLCTreeTableauWorkspaceState extends CTreeState, MainState {
}