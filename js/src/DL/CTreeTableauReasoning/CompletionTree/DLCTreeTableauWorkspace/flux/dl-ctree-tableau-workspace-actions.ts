import {WorkspaceAction} from "../../../../../Workspace/workspace";
import {AnyAction} from "../../../../../App/flux/actions";
import {CTreeAction} from "../../flux/c-tree-actions";

export namespace DL_CTREE_TABLEAU_WORKSPACE_ACTIONS {
    export const CANCEL_REASONING = "CANCEL_REASONING";
    export const INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING = "INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING";
    export const INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING = "INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING";
    export const INITIALIZE_INSTANCE_CHECKING = "INITIALIZE_INSTANCE_CHECKING";
    export const APPLY_T_RULES = "APPLY_T_RULES";
    export const APPLY_ABOX_ASSERTIONS = "APPLY_ABOX_ASSERTIONS";
    export const APPLY_CONCEPT_TABLEAU_AND_RULE = "APPLY_CONCEPT_TABLEAU_AND_RULE";
    export const APPLY_CONCEPT_TABLEAU_OR_RULE = "APPLY_CONCEPT_TABLEAU_OR_RULE";
    export const APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE = "APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE";

    export interface PAYLOAD_INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING extends CTreeAction {
        x: number,
        y: number,
        problemId: number
    }

    export interface PAYLOAD_INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING extends CTreeAction {
        x: number,
        y: number,
        problemId: number
    }

    export interface PAYLOAD_INITIALIZE_INSTANCE_CHECKING extends CTreeAction {
        x: number,
        y: number,
        problemId: number
    }

    export interface PAYLOAD_CANCEL_REASONING extends WorkspaceAction {
    }

    export interface PAYLOAD_APPLY_T_RULES extends AnyAction {
        nodeId: number,
        axiomIds: number[]
    }

    export interface PAYLOAD_APPLY_ABOX_ASSERTIONS extends CTreeAction {
    }

    export interface PAYLOAD_APPLY_CONCEPT_TABLEAU_AND_RULE extends AnyAction {
        cTreeNodeId: number,
        conceptId: number
    }

    export interface PAYLOAD_APPLY_CONCEPT_TABLEAU_OR_RULE extends AnyAction {
        cTreeNodeId: number,
        conceptId: number,
        choiceNr: number
    }

    export interface PAYLOAD_APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE extends AnyAction {
        cTreeNodeId: number,
        conceptId: number,
        newNodeX: number,
        newNodeY: number
    }

    export function CREATE_CANCEL_REASONING (workspaceId: number): PAYLOAD_CANCEL_REASONING {
        return {
            type: CANCEL_REASONING,
            workspaceId
        };
    }

    export function CREATE_INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING (x: number, y: number, problemId: number, cTreeId: number): PAYLOAD_INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING {
        return {
            type: INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING,
            x,
            y,
            problemId,
            cTreeId
        };
    }

    export function CREATE_INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING (x: number, y: number, problemId: number, cTreeId: number): PAYLOAD_INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING {
        return {
            type: INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING,
            x,
            y,
            problemId,
            cTreeId
        };
    }

    export function CREATE_INITIALIZE_INSTANCE_CHECKING (x: number, y: number, problemId: number, cTreeId: number): PAYLOAD_INITIALIZE_INSTANCE_CHECKING {
        return {
            type: INITIALIZE_INSTANCE_CHECKING,
            x,
            y,
            problemId,
            cTreeId
        };
    }

    export function CREATE_APPLY_T_RULES (nodeId: number, axiomIds: number[]): PAYLOAD_APPLY_T_RULES {
        return {
            type: APPLY_T_RULES,
            nodeId: nodeId,
            axiomIds: axiomIds
        };
    }

    export function CREATE_APPLY_ABOX_ASSERTIONS (cTreeId: number): PAYLOAD_APPLY_ABOX_ASSERTIONS {
        return {
            type: APPLY_ABOX_ASSERTIONS,
            cTreeId: cTreeId
        };
    }

    export function CREATE_APPLY_CONCEPT_TABLEAU_AND_RULE (cTreeNodeId: number, conceptId: number): PAYLOAD_APPLY_CONCEPT_TABLEAU_AND_RULE {
        return {
            type: APPLY_CONCEPT_TABLEAU_AND_RULE,
            cTreeNodeId: cTreeNodeId,
            conceptId: conceptId
        };
    }

    export function CREATE_APPLY_CONCEPT_TABLEAU_OR_RULE (cTreeNodeId: number, conceptId: number, choiceNr: number): PAYLOAD_APPLY_CONCEPT_TABLEAU_OR_RULE {
        return {
            type: APPLY_CONCEPT_TABLEAU_OR_RULE,
            cTreeNodeId: cTreeNodeId,
            conceptId: conceptId,
            choiceNr: choiceNr
        };
    }

    export function CREATE_APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE (cTreeNodeId: number, conceptId: number, newNodeX: number, newNodeY: number): PAYLOAD_APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE {
        return {
            type: APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE,
            cTreeNodeId: cTreeNodeId,
            conceptId: conceptId,
            newNodeX: newNodeX,
            newNodeY: newNodeY
        };
    }
}

export default DL_CTREE_TABLEAU_WORKSPACE_ACTIONS;