import {Reducer} from "redux";
import {DLCTreeTableauWorkspaceState} from "../dl-ctree-tableau-workspace";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "./dl-ctree-tableau-workspace-actions";
import TableauRuleRouter from "../../../../../Tableau/utils/tableau-rule-router";
import TableauReducer from "../../../../../Tableau/flux/tableau-reducer";
import ConceptSatisfiabilityChecking from "../../../../Problem/concept-satisfiability-checking";
import InstanceChecking from "../../../../Problem/instance-checking";
import {AnyAction, default as ACTIONS} from "../../../../../App/flux/actions";
import {chainReducers, mergeObjectToState, removeFromArray, setInState, unsetInState} from "../../../../../Common/utils/util";
import {default as Subsumption, SubsumptionHelper, TRuleNode} from "../../../../Denotation/KnowledgeBase/Expression/subsumption";
import ConceptRouter from "../../../../Denotation/KnowledgeBase/Expression/utils/concept-router";
import {ExistentialQuantificationTableauRuleNode} from "../../../../Denotation/KnowledgeBase/Expression/existential-quantification";
import CTREE_HELPER_FUNCTIONS from "../../utils/c-tree-helper-functions";
import {CTREE_NODE_ORIGIN} from "../../Node/c-tree-node";
import CTreeReducer from "../../flux/c-tree-reducer";
import VocabularyReducer from "../../../../Denotation/Vocabulary/flux/vocabulary-reducer";
import KnowledgeBaseReducer from "../../../../Denotation/KnowledgeBase/flux/knowledge-base-reducer";

const DLCTreeTableauWorkspaceMainReducer: Reducer<DLCTreeTableauWorkspaceState> = (state: DLCTreeTableauWorkspaceState, payload: AnyAction) => {
    payloadSwitch:
        switch (payload.type) {
            case ACTIONS.CREATE_WORKSPACE: {
                const action = payload as ACTIONS.PAYLOAD_CREATE_WORKSPACE;
                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CANCEL_REASONING: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_CANCEL_REASONING;

                state = unsetInState(state, ["Workspaces", "byId", action.workspaceId, "cTreeId"]);
                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING;
                const cTree = state.DLCTrees.byId[action.cTreeId];
                const knowledgeBase = state.DLKnowledgeBases.byId[cTree.knowledgeBaseId];

                const nodeLabel: number[] = [];

                for (let axiomId of knowledgeBase.tBoxAxiomIds) {
                    const nnfUpdate = SubsumptionHelper.getNNFExpressionUpdate(state.DLExpressions.byId[axiomId] as Subsumption, state.DLExpressions);
                    state = setInState(state, ["DLExpressions"], nnfUpdate.expressions, true);
                    nodeLabel.push(nnfUpdate.id);
                }

                [state] = CTREE_HELPER_FUNCTIONS.addNode(state, action.cTreeId, CTREE_NODE_ORIGIN.PROBLEM_INITIALIZATION, action.x, action.y, undefined, nodeLabel);

                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING;
                const problem = state.Problems.byId[action.problemId] as ConceptSatisfiabilityChecking;

                const nnfUpdate = ConceptRouter.getNNFExpressionUpdate(problem.conceptId, state.DLExpressions);

                state = setInState(state, ["DLExpressions"], nnfUpdate.expressions, true);

                [state] = CTREE_HELPER_FUNCTIONS.addNode(state, action.cTreeId, CTREE_NODE_ORIGIN.PROBLEM_INITIALIZATION, action.x, action.y, undefined, [nnfUpdate.id]);

                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.INITIALIZE_INSTANCE_CHECKING: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_INITIALIZE_INSTANCE_CHECKING;
                const problem = state.Problems.byId[action.problemId] as InstanceChecking;

                let nodeId;
                [state, nodeId] = CTREE_HELPER_FUNCTIONS.addNode(state, action.cTreeId, CTREE_NODE_ORIGIN.PROBLEM_INPUT, action.x, action.y, problem.individualSymbolId);

                const negatedNNFUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(problem.conceptId, state.DLExpressions);

                state = setInState(state, ["DLExpressions"], negatedNNFUpdate.expressions, true);
                state = CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, nodeId, negatedNNFUpdate.id);
                break payloadSwitch;

            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.APPLY_T_RULES: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_APPLY_T_RULES;
                const cTree = CTREE_HELPER_FUNCTIONS.getNodeCTree(action.nodeId, state.DLCTrees)!;

                const axiomIdsToProcess = [...action.axiomIds];

                while (axiomIdsToProcess.length > 0) {
                    const activeTableauNode = state.TableauNodes.byId[state.Tableaus.byId[cTree.tableauId].activeNodeId];

                    if (activeTableauNode.childrenIds.length === 0) {
                        break;
                    }

                    const childTableauNode = state.TableauNodes.byId[activeTableauNode.childrenIds[0]];

                    if (childTableauNode.actionType === SubsumptionHelper.TABLEAU_ACTION_TYPE &&
                        (childTableauNode as TRuleNode).cTreeNodeId === action.nodeId &&
                        axiomIdsToProcess.indexOf((childTableauNode as TRuleNode).axiomId) !== -1) {
                        state = TableauRuleRouter.applyTableauRule(childTableauNode, state.DLCTrees.byId[cTree.id].tableauId, state);
                        removeFromArray((childTableauNode as TRuleNode).axiomId, axiomIdsToProcess);
                    }
                    else {
                        break;
                    }
                }

                for (const id of axiomIdsToProcess) {
                    const tableauNodes = SubsumptionHelper.getNewApplicableTRuleNodes(action.nodeId, id, state.DLCTreeNodes, state.DLCTreeEdges, state.DLExpressions);

                    state = TableauRuleRouter.expandTableau(tableauNodes[0], state.DLCTrees.byId[cTree.id].tableauId, state);
                    state = TableauRuleRouter.applyTableauRule(tableauNodes[0], state.DLCTrees.byId[cTree.id].tableauId, state);
                }

                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTree.id, "", "none");
                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.APPLY_ABOX_ASSERTIONS: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_APPLY_ABOX_ASSERTIONS;
                state = CTREE_HELPER_FUNCTIONS.applyABoxAssertions(state, action.cTreeId);
                break payloadSwitch;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.APPLY_CONCEPT_TABLEAU_AND_RULE: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_APPLY_CONCEPT_TABLEAU_AND_RULE;
                const cTreeId = CTREE_HELPER_FUNCTIONS.getNodeCTree(action.cTreeNodeId, state.DLCTrees)!.id;

                const tableauNode = ConceptRouter.getCurrentConceptTableauRuleNodes(
                    state.Tableaus.byId[state.DLCTrees.byId[cTreeId].tableauId],
                    action.cTreeNodeId,
                    action.conceptId,
                    state.TableauNodes,
                    state.DLCTreeNodes,
                    state.DLCTreeEdges,
                    state.DLExpressions
                )[0];

                state = TableauRuleRouter.expandTableau(tableauNode, state.DLCTrees.byId[cTreeId].tableauId, state);
                state = TableauRuleRouter.applyTableauRule(tableauNode, state.DLCTrees.byId[cTreeId].tableauId, state);
                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTreeId, "", "none");
                break;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.APPLY_CONCEPT_TABLEAU_OR_RULE: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_APPLY_CONCEPT_TABLEAU_OR_RULE;
                const cTreeId = CTREE_HELPER_FUNCTIONS.getNodeCTree(action.cTreeNodeId, state.DLCTrees)!.id;

                const tableauNodes = ConceptRouter.getCurrentConceptTableauRuleNodes(
                    state.Tableaus.byId[state.DLCTrees.byId[cTreeId].tableauId],
                    action.cTreeNodeId,
                    action.conceptId,
                    state.TableauNodes,
                    state.DLCTreeNodes,
                    state.DLCTreeEdges,
                    state.DLExpressions
                );

                const tableauId = state.DLCTrees.byId[cTreeId].tableauId;

                for (const tableauNode of tableauNodes) {
                    state = TableauRuleRouter.expandTableau(tableauNode, tableauId, state);
                }

                state = TableauRuleRouter.applyTableauRule(tableauNodes[action.choiceNr], state.DLCTrees.byId[cTreeId].tableauId, state);
                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTreeId, "", "none");
                break;
            }
            case DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE: {
                const action = payload as DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.PAYLOAD_APPLY_EXISTENTIAL_QUANTIFICATION_TABLEAU_RULE;
                const cTreeId = CTREE_HELPER_FUNCTIONS.getNodeCTree(action.cTreeNodeId, state.DLCTrees)!.id;

                const tableauNode = ConceptRouter.getCurrentConceptTableauRuleNodes(
                    state.Tableaus.byId[state.DLCTrees.byId[cTreeId].tableauId],
                    action.cTreeNodeId,
                    action.conceptId,
                    state.TableauNodes,
                    state.DLCTreeNodes,
                    state.DLCTreeEdges,
                    state.DLExpressions
                )[0] as ExistentialQuantificationTableauRuleNode;

                let cTreeNodeId = tableauNode.newCTreeNodeId;
                if (Number.isNaN(cTreeNodeId)) {
                    [state, cTreeNodeId] = CTREE_HELPER_FUNCTIONS.addNode(state, cTreeId, CTREE_NODE_ORIGIN.REASONING_PROCESS, action.newNodeX, action.newNodeY);
                    tableauNode.newCTreeNodeId = cTreeNodeId;
                }

                state = mergeObjectToState(state, ["DLCTreeNodes", "byId", cTreeNodeId], {
                    x: action.newNodeX,
                    y: action.newNodeY
                });


                state = TableauRuleRouter.expandTableau(tableauNode, state.DLCTrees.byId[cTreeId].tableauId, state);
                state = TableauRuleRouter.applyTableauRule(tableauNode, state.DLCTrees.byId[cTreeId].tableauId, state);
                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTreeId, "", "none");
                break;
            }
        }
    return state;

};

const DLCTreeTableauWorkspaceReducer = chainReducers(DLCTreeTableauWorkspaceMainReducer, TableauReducer, CTreeReducer, VocabularyReducer, KnowledgeBaseReducer);

export default DLCTreeTableauWorkspaceReducer;