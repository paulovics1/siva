import * as React from "react";
import {CTreeConstants} from "../../utils/c-tree-constants";
import CTreeEdge from "../c-tree-edge";
import CTreeNode from "../../Node/c-tree-node";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import CTREE_HELPER_FUNCTIONS from "../../utils/c-tree-helper-functions";
import {getTwoPointLinePaddingCoords, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";

type CTreeEdgeViewProps = {
    cTreeEdge: CTreeEdge
    startNode: CTreeNode
    endNode: CTreeNode
    expressions: NormalizedStateIdCategory<Expression>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
}

type CTreeEdgeViewState = {}

export default class CTreeEdgeView extends React.PureComponent<CTreeEdgeViewProps, CTreeEdgeViewState> {
    constructor (props: CTreeEdgeViewProps) {
        super(props);
    }

    static getNodePadding () {
        return CTreeConstants.NODE.RADIUS.DEFAULT;
    }

    static getMarkerOverflow () {
        const strokeWidth = CTreeConstants.EDGE.STROKE_WIDTH.BASE;
        return 1.5 * strokeWidth;
    }

    getEdgePathId () {
        return `edge-path-${this.props.cTreeEdge.id}`;
    }

    render () {
        const markerOverflow = CTreeEdgeView.getMarkerOverflow();
        const nodePadding = CTreeEdgeView.getNodePadding();

        const coords = getTwoPointLinePaddingCoords(this.props.startNode.x, this.props.startNode.y, nodePadding, this.props.endNode.x, this.props.endNode.y, nodePadding + markerOverflow, 20);

        const dx = coords.target.x - coords.source.x;
        const dy = coords.target.y - coords.source.y;
        const d = (dx ** 2 + dy ** 2) ** 0.5;

        const angle = Math.atan2(dy, dx) * 180 / Math.PI;

        return (
            <g transform={`translate(${coords.source.x},${coords.source.y}) rotate(${angle})`}>
                <text className={CTreeConstants.EDGE.NAME.CLASS.BASE} textAnchor="middle">
                    <tspan dx={d / 2 + markerOverflow / 2} dy="-25">
                        {`{${CTREE_HELPER_FUNCTIONS.getEdgeLabelRoleStrings(this.props.cTreeEdge.labelRoles, this.props.expressions, this.props.vocabularySymbols).join(", ")}}`}
                    </tspan>
                </text>
                <path id={this.getEdgePathId()} className={CTreeConstants.EDGE.CLASS.BASE}
                      d={`M 0 0 Q ${d / 2} -30 ${d} 0`}/>
            </g>
        );
    }
}