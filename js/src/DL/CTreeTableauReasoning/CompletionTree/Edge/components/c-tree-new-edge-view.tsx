import * as React from "react";
import {CTreeConstants} from "../../utils/c-tree-constants";
import CTreeNode from "../../Node/c-tree-node";
import CTreeEdgeView from "./c-tree-edge-view";
import {FunctionWithId, getTwoPointLinePaddingCoords, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import CTreeDraggable from "../../utils/c-tree-draggable";

export interface EdgePoint {
    x: number,
    y: number,
    padding: number
}

type CTreeEdgeViewProps = {
    id: number,
    onMount: FunctionWithId<(cTreeNewEdgeView: CTreeNewEdgeView) => void>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
}

type CTreeEdgeViewState = {
    start?: EdgePoint | number,
    end?: EdgePoint | number
}

export default class CTreeNewEdgeView extends React.PureComponent<CTreeEdgeViewProps, CTreeEdgeViewState> implements CTreeDraggable {

    constructor (props: CTreeEdgeViewProps) {
        super(props);

        this.state = {};
    }

    onDrag (cTreeX: number, cTreeY: number): void {
        this.setEnd(cTreeX, cTreeY);
    }

    onDragEnd (): void {

    }

    setEnd (x: number, y?: number, padding: number = 0) {
        if (typeof y !== "undefined") {
            this.setState({
                end: {x, y, padding}
            });
        }
        else {
            this.setState({
                end: x
            });
        }
    }

    setStart (x: number, y?: number, padding: number = 0) {
        if (typeof y !== "undefined") {
            this.setState({
                start: {x, y, padding}
            });
        }
        else {
            this.setState({
                start: x
            });
        }
    }

    componentWillMount (): void {
        this.props.onMount(this);
    }

    render () {
        if (typeof this.state.start === "undefined" || typeof this.state.end === "undefined") {
            return null;
        }

        const markerOverflow = CTreeEdgeView.getMarkerOverflow();
        const nodePadding = CTreeEdgeView.getNodePadding();

        let startX, startY, startPadding, endX, endY, endPadding;

        if (typeof this.state.start === "number") {
            startX = this.props.cTreeNodes.byId[this.state.start].x;
            startY = this.props.cTreeNodes.byId[this.state.start].y;
            startPadding = nodePadding;
        }
        else {
            startX = this.state.start.x;
            startY = this.state.start.y;
            startPadding = this.state.start.padding;
        }

        if (typeof this.state.end === "number") {
            endX = this.props.cTreeNodes.byId[this.state.end].x;
            endY = this.props.cTreeNodes.byId[this.state.end].y;
            endPadding = nodePadding + markerOverflow;
        }
        else {
            endX = this.state.end.x;
            endY = this.state.end.y;
            endPadding = this.state.end.padding + markerOverflow;
        }

        const coords = getTwoPointLinePaddingCoords(startX, startY, startPadding, endX, endY, endPadding, 0);

        const dx = coords.target.x - coords.source.x;
        const dy = coords.target.y - coords.source.y;
        const d = (dx ** 2 + dy ** 2) ** 0.5;

        const angle = Math.atan2(dy, dx) * 180 / Math.PI;

        return (
            <g transform={`translate(${coords.source.x},${coords.source.y}) rotate(${angle})`}>
                <path className={[CTreeConstants.EDGE.CLASS.BASE, CTreeConstants.EDGE.CLASS.NEW].join(" ")}
                      d={`M 0 0 L ${d} 0`}/>
            </g>
        );
    }
}