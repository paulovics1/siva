import * as React from "react";
import CTreeView from "../../components/c-tree-view";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import CTreeNodeLabelView, {CTreeNodeLabelResizerView} from "../../Node/components/c-tree-node-label-view";
import {CTreeEventTarget} from "../../utils/c-tree-constants";
import CTreeEdgeView from "../../Edge/components/c-tree-edge-view";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";
import CTreeNodeLabelConceptView from "../../Node/components/c-tree-node-label-concept-view";
import CTreeNodeLabelTRuleButton from "../../Node/components/c-tree-node-label-t-rule-button";

export default abstract class CTreeViewMode {
    cTreeView: CTreeView;

    constructor (cTreeView: CTreeView) {
        this.cTreeView = cTreeView;

        this.handleMouseEvent = this.handleMouseEvent.bind(this);
    }

    abstract getCSSClass (): string[];

    abstract onStart (): void;

    abstract onStop (): void;

    stop () {
        this.cTreeView.setMode(undefined);
    }

    isEventConsumedByDrag (e: React.MouseEvent<any>, targetType: CTreeEventTarget, targetData: any) {
        if (e.type === "mousemove" && this.cTreeView.draggedView) {
            const draggable = this.cTreeView.draggedView;
            if (draggable && targetType === CTreeEventTarget.CTREE) {

                const coords = getSVGEventCoordinates(e);
                draggable.onDrag(coords.x, coords.y);
                return true;
            }
        }

        return false;
    }

    handleMouseEvent (e: React.MouseEvent<any>, targetType: CTreeEventTarget, targetData: any): void {
        if (this.isEventConsumedByDrag(e, targetType, targetData)) {
            return;
        }

        switch (targetType) {
            case CTreeEventTarget.CTREE:
                this.handleCTreeMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.NODE:
                this.handleNodeMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.NODE_NAME:
                this.handleNodeNameMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.NODE_LABEL:
                this.handleNodeLabelMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.NODE_LABEL_ITEM:
                if (typeof targetData.props.conceptId === "undefined") {
                    this.handleNodeLabelTRuleButtonMouseEvent(e, targetData);
                }
                else {
                    this.handleNodeLabelConceptMouseEvent(e, targetData);
                }
                break;
            case CTreeEventTarget.NODE_LABEL_RESIZE:
                this.handleNodeLabelResizerMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.EDGE:
                this.handleEdgeMouseEvent(e, targetData);
                break;
            case CTreeEventTarget.EDGE_LABEL:
                this.handleEdgeLabelMouseEvent(e, targetData);
                break;
        }

        e.preventDefault();
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {}

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {}

    handleNodeNameMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeView: CTreeNodeView): void {}

    handleNodeLabelMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeLabelView: CTreeNodeLabelView): void {}

    handleNodeLabelTRuleButtonMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeLabelTRuleButton: CTreeNodeLabelTRuleButton): void {}

    handleNodeLabelConceptMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeNodeLabelConceptView: CTreeNodeLabelConceptView): void {}

    handleNodeLabelResizerMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeLabelResizerView: CTreeNodeLabelResizerView): void {}

    handleEdgeMouseEvent (e: React.MouseEvent<SVGPathElement>, cTreeEdgeView: CTreeEdgeView): void {}

    handleEdgeLabelMouseEvent (e: React.MouseEvent<SVGTextElement>, cTreeEdgeView: CTreeEdgeView): void {}
}

export type CTreeViewModeClass = {
    new (cTreeView: CTreeView, ...data: any[]): CTreeViewMode
}