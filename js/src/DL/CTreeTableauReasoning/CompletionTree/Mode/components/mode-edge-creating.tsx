import CTreeViewMode from "./c-tree-view-mode";
import {default as CTreeView} from "../../components/c-tree-view";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import * as React from "react";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";
import CTREE_ACTIONS from "../../flux/c-tree-actions";
import CTreeNewEdgeView from "../../Edge/components/c-tree-new-edge-view";
import {CTreeEventTarget} from "../../utils/c-tree-constants";

export default class ModeEdgeCreating extends CTreeViewMode {
    private newEdgeView?: CTreeNewEdgeView;

    getCSSClass (): string[] {
        return ["mode-edge-create"];
    }

    protected createEdge (startNodeId: number, endNodeId: number) {
        this.cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_CREATE_EDGE(this.cTreeView.id, startNodeId, endNodeId));
        this.stop();
    }

    protected beginEdgeCreating (startNodeId: number) {
        this.cTreeView.toggleEdgeCreation(cTreeNewEdgeView => {
            cTreeNewEdgeView.setStart(startNodeId);
            cTreeNewEdgeView.setEnd(startNodeId);
            this.newEdgeView = cTreeNewEdgeView;
            this.cTreeView.setDraggedView(cTreeNewEdgeView);
        });
    }

    isEventConsumedByDrag (e: React.MouseEvent<any>, targetType: CTreeEventTarget, targetData: any) {
        if (this.newEdgeView) {
            if (targetType === CTreeEventTarget.CTREE) {
                if (e.type === "mousemove") {
                    if (e.buttons & 1) {
                        const coords = getSVGEventCoordinates(e);
                        this.newEdgeView.setEnd(coords.x, coords.y);
                        return true;
                    }
                }
            }
            if (targetType === CTreeEventTarget.NODE) {
                if (e.type === "mouseup") {
                    if (e.button === 0) {
                        this.createEdge(this.newEdgeView.state.start as number, (targetData as CTreeNodeView).cTreeNode.id);
                        return true;
                    }
                }
            }
        }
        return false;
    }

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {
        if (!this.newEdgeView) {
            switch (e.type) {
                case "mousedown": {
                    if (e.button === 0) {
                        this.beginEdgeCreating(cTreeNodeView.cTreeNode.id);
                    }
                    e.stopPropagation();
                }
            }
        }
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {
        if (this.newEdgeView) {
            switch (e.type) {
                case "mousedown":
                case "mouseup": {
                    if (e.button === 0) {
                        this.reset();
                    }
                    break;
                }
            }
        }
    }

    onStart (): void {

    }

    onStop (): void {
        this.reset();
    }

    reset (): void {
        this.cTreeView.setDraggedView(undefined);

        if (this.newEdgeView) {
            this.cTreeView.toggleEdgeCreation(this.newEdgeView.props.id);
            delete this.newEdgeView;
        }
    }
}