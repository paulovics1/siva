import CTreeViewMode from "./c-tree-view-mode";
import * as React from "react";
import CTreeView from "../../components/c-tree-view";
import CTreeNodeView from "../../Node/components/c-tree-node-view";
import CTREE_ACTIONS from "../../flux/c-tree-actions";
import CTreeNewNodeView from "../../Node/components/c-tree-new-node-view";
import CTREE_HELPER_FUNCTIONS from "../../utils/c-tree-helper-functions";
import {CTREE_NODE_ORIGIN} from "../../Node/c-tree-node";
import {getSVGEventCoordinates} from "../../../../../Common/utils/util";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";

export default class ModeKnowledgeBaseInitialization extends CTreeViewMode {
    protected individualSymbolIdsToInitialize: number[];
    protected newNodeView?: CTreeNewNodeView;

    getCSSClass (): string[] {
        return ["mode-abox-initialization"];
    }

    createNode (x: number, y: number, cTreeId: number) {
        this.cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_CREATE_NODE(cTreeId, CTREE_NODE_ORIGIN.KNOWLEDGE_BASE_INITIALIZATION, x, y, this.individualSymbolIdsToInitialize[0]));
        this.cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_ABOX_ASSERTIONS(cTreeId));
        this.proceedToNextIndividual();
    }

    handleCTreeMouseEvent (e: React.MouseEvent<SVGSVGElement>, cTreeView: CTreeView): void {
        switch (e.type) {
            case "mousemove": {
                if (this.newNodeView) {
                    const coords = getSVGEventCoordinates(e);
                    this.newNodeView.setPosition(coords.x, coords.y);
                }
                break;
            }
            case "click": {
                if (e.button === 0) {
                    const coords = getSVGEventCoordinates(e);
                    this.createNode(coords.x, coords.y, cTreeView.id);
                }
                break;
            }
        }
    }

    handleNodeMouseEvent (e: React.MouseEvent<SVGCircleElement>, cTreeNodeView: CTreeNodeView): void {
        e.stopPropagation();
    }

    proceedToNextIndividual () {
        this.individualSymbolIdsToInitialize.shift();

        if (this.individualSymbolIdsToInitialize.length > 0) {
            this.setNodeName();
        }
        else {
            this.stop();
        }
    }

    setNodeName () {
        const nodeName = this.cTreeView.props.filteredVocabularySymbols.byId[this.individualSymbolIdsToInitialize[0]].symbol;
        this.newNodeView!.setName(nodeName);
        this.cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_SET_STATUS(this.cTreeView.id, `[Knowledge base initialization] Click to place a new completion tree node, that will represent individual ${nodeName}.`, "neutral"));
    }

    onStart (): void {
        this.individualSymbolIdsToInitialize = CTREE_HELPER_FUNCTIONS.getIndividualsToInitializeInCTree(
            this.cTreeView.props.knowledgeBase,
            this.cTreeView.props.expressions,
            this.cTreeView.props.filteredCTreeNodes
        );

        if (this.individualSymbolIdsToInitialize.length > 0) {
            this.cTreeView.toggleNodeCreation(cTreeNewNodeView => {
                this.newNodeView = cTreeNewNodeView;
                this.setNodeName();
            });
        }
    }

    onStop (): void {
        if (this.newNodeView) {
            this.cTreeView.toggleNodeCreation(this.newNodeView.props.id);
            delete this.newNodeView;
        }
    }
}