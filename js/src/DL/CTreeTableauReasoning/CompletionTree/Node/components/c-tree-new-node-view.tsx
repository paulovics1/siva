import * as React from "react";
import {CTreeConstants} from "../../utils/c-tree-constants";
import {FunctionWithId} from "../../../../../Common/utils/util";

type CTreeNodeViewProps = {
    id: number,
    onMount: FunctionWithId<(cTreeNewNodeView: CTreeNewNodeView) => void>
}

type CTreeNodeViewState = {
    name?: string,
    x?: number,
    y?: number
}

export default class CTreeNewNodeView extends React.PureComponent<CTreeNodeViewProps, CTreeNodeViewState> {

    constructor (props: CTreeNodeViewProps) {
        super(props);

        this.state = {};
    }

    setPosition (x?: number, y?: number) {
        this.setState({
            x,
            y
        });
    }

    setName (name?: string) {
        this.setState({
            name
        });
    }

    componentWillMount (): void {
        this.props.onMount(this);
    }

    render () {
        if (typeof this.state.x === "undefined" || typeof this.state.y === "undefined") {
            return null;
        }

        return (
            <g transform={`translate(${this.state.x},${this.state.y})`}>
                <text
                    className={CTreeConstants.NODE.NAME.CLASS.BASE}
                    dx={CTreeConstants.NODE.NAME.D.X.DEFAULT}
                    dy={CTreeConstants.NODE.NAME.D.Y.DEFAULT}
                >
                    {this.state.name}
                </text>
                <circle
                    className={[CTreeConstants.NODE.CLASS.BASE, CTreeConstants.NODE.CLASS.NEW].join(" ")}
                    r={CTreeConstants.NODE.RADIUS.DEFAULT}
                >
                    <animate
                        attributeName="opacity"
                        values="1; 0.7; 1"
                        keyTimes="0; 0.5; 1"
                        dur="0.75"
                        repeatCount="indefinite"
                    />
                </circle>
            </g>
        );
    }
}