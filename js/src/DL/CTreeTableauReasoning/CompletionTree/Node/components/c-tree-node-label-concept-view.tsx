import * as React from "react";
import {CTreeConstants, CTreeEventTarget} from "../../utils/c-tree-constants";
import {getRoundedRectanglePath, NormalizedStateIdCategory} from "../../../../../Common/utils/util";
import {default as CTreeView, PopoverData} from "../../components/c-tree-view";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import CTreeNode from "../c-tree-node";
import CTreeEdge from "../../Edge/c-tree-edge";
import {Dispatch} from "redux";
import {CTreeContradictions} from "../../utils/c-tree-helper-functions";
import CTreeViewMode from "../../Mode/components/c-tree-view-mode";
import MainState from "../../../../../App/flux/main-state";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";
import ConceptRouter from "../../../../Denotation/KnowledgeBase/Expression/utils/concept-router";

export interface CTreeNodeLabelConceptViewProps {
    wrappedTextData: WrappedTextData[]
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent
    conceptId: number
    cTreeId: number
    cTreeNodeId: number
    expressions: NormalizedStateIdCategory<Expression>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    dispatch: Dispatch<MainState>
    cTreeContradictions: CTreeContradictions
    cTreeNodeBlockedBy: number | undefined
}

export interface WrappedTextData {
    text: string,
    x: number,
    y: number,
    width: number,
    textHeight: number
}

export default class CTreeNodeLabelConceptView extends React.PureComponent<CTreeNodeLabelConceptViewProps> {
    firstBlock: SVGPathElement;
    classes: string[] = [];

    getPopoverData (): PopoverData {
        const firstBlockBBox = this.firstBlock.getBoundingClientRect();
        let text = (<div>
            <span>
                {
                    ConceptRouter.getConceptTableauRuleDescription(this.props.conceptId, this.props.cTreeNodes.byId[this.props.cTreeNodeId], this.props.expressions, this.props.vocabularySymbols)
                }
            </span>
            <br/>
            {
                Object.keys(this.props.cTreeContradictions).length !== 0 ?
                    <span style={{color: "red"}}>Tableau rules can not be applied on a completion tree that contains a clash.</span> :
                    null
            }
        </div>);

        return {
            positionLeft: firstBlockBBox.left + firstBlockBBox.width / 2,
            positionTop: firstBlockBBox.top,
            placement: "top",
            title: "Tableau rule",
            children: text
        };
    }

    updateClasses (props: CTreeNodeLabelConceptViewProps) {
        const concept = props.expressions.byId[props.conceptId];
        const conceptHelper = ConceptRouter.getHelper(concept.type)!;

        this.classes = [
            `tableau-node-${conceptHelper.TABLEAU_NODE_TYPE.toLowerCase()}`
        ];

        if (ConceptRouter.isTableauRuleApplicable(props.cTreeNodeId, props.conceptId, props.cTreeNodes, props.cTreeEdges, props.expressions)) {
            this.classes.push('concept-expandable');
        }

        if (props.cTreeContradictions[props.cTreeNodeId] &&
            props.cTreeContradictions[props.cTreeNodeId].indexOf(props.conceptId) !== -1) {
            this.classes.push('concept-clash');
        }
    }

    componentWillUpdate (nextProps: Readonly<CTreeNodeLabelConceptViewProps>, nextState: Readonly<{}>, nextContext: any): void {
        this.updateClasses(nextProps);
    }

    componentWillMount (): void {
        this.updateClasses(this.props);
    }

    onExpand (cTreeView: CTreeView) {
        if (Object.keys(this.props.cTreeContradictions).length === 0 && typeof this.props.cTreeNodeBlockedBy === "undefined") {
            ConceptRouter.onConceptTableauExpand(this.props.cTreeId, this.props.cTreeNodeId, this.props.conceptId, cTreeView);
        }
    }

    render () {
        const svgElements = [];

        const nodeLabelConceptMouseEventHandler = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE_LABEL_ITEM, this);

        for (let i = 0; i < this.props.wrappedTextData.length; i++) {
            const data = this.props.wrappedTextData[i];
            svgElements.push(
                <path
                    key={`path-${data.text}-${i}`}
                    {...(i === 0 ?
                            {
                                ref: block => {
                                    if (block) {
                                        this.firstBlock = block;
                                    }
                                }
                            } :
                            {}
                    )}
                    d={getRoundedRectanglePath(
                        data.x,
                        data.y,
                        data.width + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT,
                        data.textHeight + 2 * CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT,
                        i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === this.props.wrappedTextData.length - 1 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0,
                        i === 0 ? CTreeConstants.NODE.LABEL.CONCEPT.RADIUS.DEFAULT : 0)}
                    onMouseEnter={nodeLabelConceptMouseEventHandler}
                    onMouseLeave={nodeLabelConceptMouseEventHandler}
                    onClick={nodeLabelConceptMouseEventHandler}
                />
            );
            svgElements.push(
                <text
                    textAnchor="middle"
                    dominantBaseline="text-before-edge"
                    key={`text-${data.text}-${i}`}
                    x={data.x + (data.width / 2) + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.HORIZONTAL.DEFAULT}
                    y={data.y + CTreeConstants.NODE.LABEL.CONCEPT.PADDING.VERTICAL.DEFAULT}
                    pointerEvents="none"
                >
                    {data.text}
                </text>
            );
        }
        return (
            <g
                className={["ctree-node-label-concept-group", ...this.classes].join(" ")}
            >
                {svgElements}
            </g>
        );
    }
}