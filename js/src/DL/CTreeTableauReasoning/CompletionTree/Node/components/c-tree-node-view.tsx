import * as React from "react";
import {mapNormalizedStateIdCategory, NormalizedStateIdCategory, preventDefault} from "../../../../../Common/utils/util";
import CTreeNodeRenameModal, {CTreeNodeRenameModalState} from "./c-tree-node-rename-modal";
import {default as CTreeView} from "../../components/c-tree-view";
import {CTreeConstants, CTreeEventTarget} from "../../utils/c-tree-constants";
import autobind from "autobind-decorator";
import CTreeNodeLabelView from "./c-tree-node-label-view";
import CTreeDraggable from "../../utils/c-tree-draggable";
import Latex from "react-latex";
import {Dispatch} from "redux";
import CTREE_ACTIONS from "../../flux/c-tree-actions";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import CTreeEdge from "../../Edge/c-tree-edge";
import CTREE_HELPER_FUNCTIONS, {CTreeContradictions} from "../../utils/c-tree-helper-functions";
import CTreeNode, {CTREE_NODE_ORIGIN} from "../c-tree-node";
import CTreeViewMode from "../../Mode/components/c-tree-view-mode";
import {MainComponent} from "../../../../../App/components/main-component";
import MainState from "../../../../../App/flux/main-state";
import TRuleAxiomChoiceModal, {TRuleAxiomChoiceModalData} from "./t-rule-axiom-choice-modal";
import TableauNode from "../../../../../Tableau/tableau-node";
import {Tableau} from "../../../../../Tableau/tableau";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";

type CTreeNodeViewProps = {
    cTreeNodeId: number
    expressions: NormalizedStateIdCategory<Expression>
    tBoxAxiomIds: number[]
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    onMouseEvent: typeof CTreeViewMode.prototype.handleMouseEvent
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    fillAndShowContextMenu: typeof MainComponent.prototype.fillAndShowContextMenu
    cTreeId: number
    measureSVGText: typeof CTreeView.prototype.measureSVGText
    dispatch: Dispatch<MainState>
    cTreeContradictions: CTreeContradictions
    cTreeNodeBlockedBy: number | undefined
    tableau: Tableau
    tableauNodes: NormalizedStateIdCategory<TableauNode>
}

type CTreeNodeViewState = {}

export default class CTreeNodeView extends React.Component<CTreeNodeViewProps, CTreeNodeViewState> implements CTreeDraggable {
    constructor (props: CTreeNodeViewProps) {
        super(props);
    }

    get cTreeNode () {
        return this.props.cTreeNodes.byId[this.props.cTreeNodeId];
    }

    get id () {
        return this.cTreeNode.id;
    }

    get x () {
        return this.cTreeNode.x;
    }

    get y () {
        return this.cTreeNode.y;
    }

    getNewApplicableTableauTRuleNodes () {
        return CTREE_HELPER_FUNCTIONS.getNewApplicableTableauTRuleNodes(this.id, this.props.tBoxAxiomIds, this.props.cTreeNodes, this.props.cTreeEdges, this.props.expressions);
    }

    @autobind
    dispatchRename (newName: string) {
        this.props.dispatch(CTREE_ACTIONS.CREATE_RENAME_NODE(this.props.cTreeId, this.id, newName));
    }

    @autobind
    dispatchMove (x: number, y: number) {
        this.props.dispatch(CTREE_ACTIONS.CREATE_MOVE_NODE(this.props.cTreeId, this.id, x, y));
    }

    showRenameModal () {
        if (!this.isNamedIndividual()) {
            this.props.fillAndShowModal(
                CTreeNodeRenameModal,
                {
                    id: this.id,
                    originalName: this.cTreeNode.nameOrIndividualSymbolId,
                    takenNames: new Set(mapNormalizedStateIdCategory(this.props.cTreeNodes,
                        node => CTREE_HELPER_FUNCTIONS.getNodeName(node.nameOrIndividualSymbolId, this.props.vocabularySymbols))),
                    cTreeId: this.props.cTreeId,
                    submitHandler: this.dispatchRename
                } as Partial<CTreeNodeRenameModalState>
            );
        }
    }

    fillAndShowTRuleAxiomChoiceModal () {
        this.props.fillAndShowModal(TRuleAxiomChoiceModal, {
            tableauId: this.props.tableau.id,
            activeTableauNodeId: this.props.tableau.activeNodeId,
            tableauNodes: this.props.tableauNodes,
            cTreeNodeId: this.id,
            expressions: this.props.expressions,
            vocabularySymbols: this.props.vocabularySymbols,
            tBoxAxiomIds: this.props.tBoxAxiomIds,
            cTreeNodes: this.props.cTreeNodes,
            cTreeEdges: this.props.cTreeEdges,
            dispatch: this.props.dispatch
        } as TRuleAxiomChoiceModalData);
    }

    showContextMenu (pageX: number, pageY: number) {
        this.props.fillAndShowContextMenu({
            x: pageX,
            y: pageY,
            contextMenuRowDataList: [
                {
                    title: <span>Apply <Latex children="$\\mathcal{T}\\!$"/>-rule</span>,
                    disabled: this.doesContainClash() || this.isBlocked() || !this.applicableTRuleExists(),
                    action: () => {
                        this.fillAndShowTRuleAxiomChoiceModal();
                    }
                }
            ]
        });
    }

    onDrag (cTreeX: number, cTreeY: number): void {
        this.dispatchMove(cTreeX, cTreeY);
    }

    onDragEnd (): void {}

    isNamedIndividual () {
        return typeof this.cTreeNode.nameOrIndividualSymbolId === "number";
    }

    doesContainClash () {
        return typeof this.props.cTreeContradictions[this.id] !== "undefined" && this.props.cTreeContradictions[this.id].length > 0;
    }

    isBlocked () {
        return typeof this.props.cTreeNodeBlockedBy !== "undefined";
    }

    applicableTRuleExists () {
        return this.getNewApplicableTableauTRuleNodes().length > 0;
    }

    getClassName () {
        const result = [];

        if (this.getNewApplicableTableauTRuleNodes().length > 0) {
            result.push(CTreeConstants.NODE.GROUP.CLASS.T_RULE_APPLICABLE);
        }

        if (this.doesContainClash()) {
            result.push(CTreeConstants.NODE.GROUP.CLASS.CLASH);
        }

        if (this.isBlocked()) {
            result.push(CTreeConstants.NODE.GROUP.CLASS.BLOCKED);
        }

        return result.join(" ");
    }

    shouldComponentUpdate (nextProps: Readonly<CTreeNodeViewProps>, nextState: Readonly<{}>, nextContext: any): boolean {
        return this.props.cTreeNodeBlockedBy !== nextProps.cTreeNodeBlockedBy ||
            this.props.cTreeContradictions !== nextProps.cTreeContradictions ||
            this.props.measureSVGText !== nextProps.measureSVGText ||
            this.props.vocabularySymbols !== nextProps.vocabularySymbols ||
            this.props.onMouseEvent !== nextProps.onMouseEvent ||
            this.props.cTreeNodes.byId[this.props.cTreeNodeId] !== nextProps.cTreeNodes.byId[nextProps.cTreeNodeId] ||
            this.props.cTreeNodes.allIds !== nextProps.cTreeNodes.allIds ||
            Boolean(this.props.cTreeNodes.allIds.find(oldId => this.props.cTreeNodes.byId[oldId].labelConcepts !== nextProps.cTreeNodes.byId[oldId].labelConcepts)) ||
            this.props.cTreeEdges.allIds !== nextProps.cTreeEdges.allIds ||
            Boolean(this.props.cTreeEdges.allIds.find(oldId => this.props.cTreeEdges.byId[oldId].labelRoles !== nextProps.cTreeEdges.byId[oldId].labelRoles));
    }

    render () {
        const nodeMouseEvent = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE, this);
        const nameMouseEvent = (e: React.MouseEvent<Element>) => this.props.onMouseEvent(e, CTreeEventTarget.NODE_NAME, this);

        return (
            <g
                className={this.getClassName()}
                transform={`translate(${this.x},${this.y})`}
            >
                <text
                    className={CTreeConstants.NODE.NAME.CLASS.BASE}
                    dx={CTreeConstants.NODE.NAME.D.X.DEFAULT}
                    dy={CTreeConstants.NODE.NAME.D.Y.DEFAULT}
                    onClick={nameMouseEvent}
                    onContextMenu={nameMouseEvent}
                    onMouseMove={nameMouseEvent}
                    onMouseDown={nameMouseEvent}
                    onMouseUp={nameMouseEvent}
                >
                    {CTREE_HELPER_FUNCTIONS.getNodeName(this.cTreeNode.nameOrIndividualSymbolId, this.props.vocabularySymbols)}
                    {this.cTreeNode.origin !== CTREE_NODE_ORIGIN.PROBLEM_INPUT ||
                    <tspan fontWeight="bold"> (problem input)</tspan>}
                </text>
                {
                    // this.isBlocked() ||
                    // this.doesContainClash() ||
                    // !this.applicableTRuleExists() ||
                    // <circle
                    //     fill="#0095ff"
                    //     stroke="black"
                    //     r={CTreeConstants.NODE.RADIUS.DEFAULT + CTreeConstants.NODE.T_RULE_APPLICABLE.EXTRA_RADIUS.DEFAULT}
                    //     pointerEvents="none"
                    // />
                }
                <circle
                    className={CTreeConstants.NODE.CLASS.BASE}
                    r={CTreeConstants.NODE.RADIUS.DEFAULT}
                    onClick={nodeMouseEvent}
                    onContextMenu={nodeMouseEvent}
                    onMouseMove={nodeMouseEvent}
                    onMouseDown={nodeMouseEvent}
                    onMouseUp={nodeMouseEvent}
                    onDragStart={preventDefault}
                />
                {
                    !this.isNamedIndividual() ||
                    <circle
                        fill="none"
                        r={CTreeConstants.NODE.RADIUS.DEFAULT - CTreeConstants.NODE.INDIVIDUAL.STROKE_WIDTH.DEFAULT * 1.5}
                        stroke={CTreeConstants.NODE.INDIVIDUAL.STROKE.DEFAULT}
                        strokeWidth={CTreeConstants.NODE.INDIVIDUAL.STROKE_WIDTH.DEFAULT}
                        pointerEvents="none"
                    />
                }
                {
                    this.isBlocked() ?
                        <use
                            xlinkHref="#svg-lock"
                            pointerEvents="none"
                        /> :
                        null
                }
                <CTreeNodeLabelView
                    cTreeId={this.props.cTreeId}
                    expressions={this.props.expressions}
                    cTreeNodes={this.props.cTreeNodes}
                    cTreeEdges={this.props.cTreeEdges}
                    vocabularySymbols={this.props.vocabularySymbols}
                    cTreeNodeId={this.props.cTreeNodeId}
                    measureSVGText={this.props.measureSVGText}
                    onMouseEvent={this.props.onMouseEvent}
                    dispatch={this.props.dispatch}
                    applicableTRuleExists={!this.isBlocked() && !this.doesContainClash() && this.applicableTRuleExists()}
                    cTreeContradictions={this.props.cTreeContradictions}
                    cTreeNodeBlockedBy={this.props.cTreeNodeBlockedBy}
                />
            </g>
        );
    }
}