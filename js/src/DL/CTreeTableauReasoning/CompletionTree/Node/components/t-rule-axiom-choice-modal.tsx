import * as React from "react";
import {SyntheticEvent} from "react";
import autobind from "autobind-decorator";
import {Button, ListGroup, ListGroupItem, Modal} from "react-bootstrap";
import Latex from "react-latex";
import MainState from "../../../../../App/flux/main-state";
import {Dispatch} from "redux";
import {NormalizedStateIdCategory, removeFromArray} from "../../../../../Common/utils/util";
import {VocabularySymbol} from "../../../../Denotation/Vocabulary/vocabulary-symbol";
import {ModalComponentProps, ModalComponentState} from "../../../../../Common/modal-component";
import CTREE_HELPER_FUNCTIONS from "../../utils/c-tree-helper-functions";
import CTreeNode from "../c-tree-node";
import CTreeEdge from "../../Edge/c-tree-edge";
import TableauNode from "../../../../../Tableau/tableau-node";
import ConfirmationModal from "../../../../../Common/components/confirmation-modal";
import TABLEAU_ACTIONS from "../../../../../Tableau/flux/tableau-actions";
import {SubsumptionHelper, TRuleNode} from "../../../../Denotation/KnowledgeBase/Expression/subsumption";
import Expression from "../../../../Denotation/KnowledgeBase/Expression/expression";
import ExpressionRouter from "../../../../Denotation/KnowledgeBase/Expression/utils/expression-router";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";

export interface TRuleAxiomChoiceModalProps extends TRuleAxiomChoiceModalData, ModalComponentProps {
}

export interface TRuleAxiomChoiceModalState extends ModalComponentState {
    selectedAxiomIds: number[]
    newApplicableTableauTRuleNodes: TRuleNode[]
}

export type TRuleAxiomChoiceModalData = {
    tableauId: number,
    activeTableauNodeId: number,
    tableauNodes: NormalizedStateIdCategory<TableauNode>
    expressions: NormalizedStateIdCategory<Expression>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    cTreeNodes: NormalizedStateIdCategory<CTreeNode>
    cTreeEdges: NormalizedStateIdCategory<CTreeEdge>
    tBoxAxiomIds: number[]
    cTreeNodeId: number
    dispatch: Dispatch<MainState>
}

export default class TRuleAxiomChoiceModal extends React.PureComponent<TRuleAxiomChoiceModalProps, TRuleAxiomChoiceModalState> {
    constructor (props: TRuleAxiomChoiceModalProps) {
        super(props);

        this.state = {
            open: true,
            selectedAxiomIds: [],
            newApplicableTableauTRuleNodes: CTREE_HELPER_FUNCTIONS.getNewApplicableTableauTRuleNodes(this.props.cTreeNodeId, this.props.tBoxAxiomIds, this.props.cTreeNodes, this.props.cTreeEdges, this.props.expressions)
        } as any as TRuleAxiomChoiceModalState;

    }

    protected onSubmit (): void {
        if (!SubsumptionHelper.doActiveTableauNodeChildrenMatchTRules(this.state.selectedAxiomIds, this.props.cTreeNodeId, this.props.activeTableauNodeId, this.props.tableauNodes)) {
            this.props.fillAndShowModal(ConfirmationModal, {
                title: "Confirmation of different tableau rule application order",
                message: "You are about to apply tableau rules in a different order than in the previous traversal. This will invalidate the part of the tableau that does not match.",
                onConfirm: () => {
                    this.props.dispatch(TABLEAU_ACTIONS.CREATE_PRUNE_ACTIVE_CHILDREN(this.props.tableauId));
                    this.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_T_RULES(this.props.cTreeNodeId, this.state.selectedAxiomIds));
                }
            });
        }
        else {
            this.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_T_RULES(this.props.cTreeNodeId, this.state.selectedAxiomIds));
        }
    }

    @autobind
    handleOk (e?: SyntheticEvent<any>) {
        !e || e.preventDefault();
        this.onSubmit();
        this.hide();
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    show () {
        this.setState({
            open: true
        });
    }

    @autobind
    toggleAxiom (axiomId: number) {
        this.setState(prevState => {
            const result = [...prevState.selectedAxiomIds];

            if (!removeFromArray(axiomId, result)) {
                result.push(axiomId);
            }

            return {
                selectedAxiomIds: result
            };

        });
    }

    static T () {
        return <Latex children="$\\mathcal{T}\\!$"/>;
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    render () {
        const cTreeNode = this.props.cTreeNodes.byId[this.props.cTreeNodeId];

        return (
            <Modal
                show={this.state.open}
                onHide={this.hide}
                bsSize="large"
                onExited={this.removeModal}
                backdrop
            >
                <form onSubmit={this.handleOk}>
                    <Modal.Header closeButton>
                        <Modal.Title>Apply {TRuleAxiomChoiceModal.T()}-rules
                            in {CTREE_HELPER_FUNCTIONS.getNodeName(cTreeNode.nameOrIndividualSymbolId, this.props.vocabularySymbols)}</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <ListGroup>
                            {
                                this.state.newApplicableTableauTRuleNodes.map((tableauTRuleNode) => {
                                    const simpleString = ExpressionRouter.getSimpleString(tableauTRuleNode.axiomId, this.props.expressions, this.props.vocabularySymbols);
                                    const latexString = ExpressionRouter.getLatexString(tableauTRuleNode.axiomId, this.props.expressions, this.props.vocabularySymbols);
                                    return (
                                        <ListGroupItem
                                            key={simpleString}
                                            onClick={() => {this.toggleAxiom(tableauTRuleNode.axiomId);}}
                                            title={simpleString}
                                            className={`checkbox-list-item ${this.state.selectedAxiomIds.indexOf(tableauTRuleNode.axiomId) !== -1 ? "selected" : ""}`}
                                        >
                                            <div
                                                className="latex-list-group-item"
                                            >
                                                <div
                                                    className="latex-ellipsis">
                                                    <Latex
                                                        children={`$${latexString}$`}/>
                                                </div>
                                                <div>
                                                    <Latex
                                                        children={`$\\mathcal{T${this.props.tBoxAxiomIds.indexOf(tableauTRuleNode.axiomId) + 1}}$`}/>
                                                </div>
                                            </div>

                                        </ListGroupItem>
                                    );
                                })
                            }
                        </ListGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                        >
                            Apply {TRuleAxiomChoiceModal.T()}-rule{this.state.selectedAxiomIds.length <= 1 || "s"}
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}