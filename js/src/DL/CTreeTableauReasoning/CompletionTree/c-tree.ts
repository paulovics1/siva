import CTreeEdge from "./Edge/c-tree-edge";
import CTreeNode from "./Node/c-tree-node";
import {createNewEmptyTableau, TableauState} from "../../../Tableau/tableau";
import {VocabularySymbol} from "../../Denotation/Vocabulary/vocabulary-symbol";
import {appendNormalizedToState, NormalizedStateIdCategory} from "../../../Common/utils/util";
import Expression from "../../Denotation/KnowledgeBase/Expression/expression";
import {DLDenotationState} from "../../Denotation/denotation";

export default interface CTree {
    id: number
    knowledgeBaseId: number
    problemId: number
    tableauId: number
    nodes: number[]
    edges: number[]
    statusType: CTreeStatusBarStatusType
    statusMessage: string
    reasoningProcessStage: CTreeReasoningProcessStage
}

export function createNewEmptyCTree<S extends CTreeState> (state: S, knowledgeBaseId: number, problemId: number) {
    let newTableau;

    [state, newTableau] = createNewEmptyTableau(state, CTreeTableauType);

    const result = {
        id: NaN,
        knowledgeBaseId,
        problemId,
        tableauId: newTableau.id,
        nodes: [],
        edges: [],
        statusType: "neutral",
        statusMessage: "Completion tree has been created",
        reasoningProcessStage: CTreeReasoningProcessStage.PROBLEM_INTIALIZATION
    } as CTree;

    state = appendNormalizedToState(state, ["DLCTrees"], result);

    return [state, result] as [S, CTree];
}

export const CTreeTableauType = "CTreeTableau";

export type CTreeStatusBarStatusType = "neutral" | "positive" | "negative" | "none"

export enum CTreeReasoningProcessStage {
    PROBLEM_INTIALIZATION = 0,
    KNOWLEDGE_BASE_INITIALIZATION = 1,
    TABLEAU_RULE_APPLICATION = 2
}

export interface CTreeTableauNodeDescriptionState {
    DLCTreeNodes: NormalizedStateIdCategory<CTreeNode>,
    DLExpressions: NormalizedStateIdCategory<Expression>,
    DLVocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
}

export interface CTreeState extends DLDenotationState, TableauState {
    DLCTrees: NormalizedStateIdCategory<CTree>,
    DLCTreeNodes: NormalizedStateIdCategory<CTreeNode>,
    DLCTreeEdges: NormalizedStateIdCategory<CTreeEdge>
}