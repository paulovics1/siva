import {CTREE_NODE_ORIGIN} from "../Node/c-tree-node";
import {AnyAction} from "../../../../App/flux/actions";
import {CTreeStatusBarStatusType} from "../c-tree";

export namespace CTREE_ACTIONS {
    export const SET_STATUS = "SET_STATUS";
    export const SET_PROBLEM = "SET_PROBLEM";
    export const CREATE_NODE = "CREATE_NODE";
    export const RENAME_NODE = "RENAME_NODE";
    export const MOVE_NODE = "MOVE_NODE";
    export const CREATE_EDGE = "CREATE_EDGE";

    export interface PAYLOAD_SET_PROBLEM extends CTreeAction {
        problemId: number
    }

    export interface PAYLOAD_CREATE_EDGE extends CTreeAction {
        startNodeId: number
        endNodeId: number
    }

    export interface PAYLOAD_CREATE_NODE extends CTreeAction {
        nameOrIndividualSymbolId?: string | number,
        nodeOrigin: CTREE_NODE_ORIGIN,
        x: number,
        y: number,
        nodeLabelConceptIds?: number[]
    }

    export interface PAYLOAD_RENAME_NODE extends CTreeAction {
        nodeId: number,
        name: string
    }

    export interface PAYLOAD_MOVE_NODE extends CTreeAction {
        nodeId: number,
        x: number,
        y: number
    }

    export interface PAYLOAD_SET_STATUS extends CTreeAction {
        statusMessage: string
        statusType: CTreeStatusBarStatusType
    }

    export function CREATE_SET_STATUS (cTreeId: number, statusMessage: string, statusType: CTreeStatusBarStatusType): PAYLOAD_SET_STATUS {
        return {
            type: SET_STATUS,
            cTreeId,
            statusMessage,
            statusType
        };
    }

    export function CREATE_SET_PROBLEM (cTreeId: number, problemId: number): PAYLOAD_SET_PROBLEM {
        return {
            type: SET_PROBLEM,
            cTreeId,
            problemId
        };
    }

    export function CREATE_CREATE_NODE (cTreeId: number, nodeOrigin: CTREE_NODE_ORIGIN, x: number, y: number, nameOrIndividualSymbolId?: string | number, nodeLabelConceptIds?: number[]): PAYLOAD_CREATE_NODE {
        return {
            type: CREATE_NODE,
            cTreeId,
            nameOrIndividualSymbolId,
            nodeOrigin,
            x,
            y,
            nodeLabelConceptIds
        };
    }

    export function CREATE_RENAME_NODE (cTreeId: number, nodeId: number, name: string): PAYLOAD_RENAME_NODE {
        return {
            type: RENAME_NODE,
            cTreeId,
            nodeId,
            name
        };
    }

    export function CREATE_MOVE_NODE (cTreeId: number, nodeId: number, x: number, y: number): PAYLOAD_MOVE_NODE {
        return {
            type: MOVE_NODE,
            cTreeId,
            nodeId,
            x: x,
            y: y
        };
    }

    export function CREATE_CREATE_EDGE (cTreeId: number, startNodeId: number, endNodeId: number): PAYLOAD_CREATE_EDGE {
        return {
            type: CREATE_EDGE,
            cTreeId,
            startNodeId,
            endNodeId
        };
    }
}

export interface CTreeAction extends AnyAction {
    cTreeId: number
}

export default CTREE_ACTIONS;