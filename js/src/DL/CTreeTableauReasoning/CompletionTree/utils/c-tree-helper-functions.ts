import {CTreeReasoningProcessStage, CTreeState, CTreeStatusBarStatusType, default as CTree} from "../c-tree";
import {appendNormalizedToState, appendToStateArray, filterNormalizedStateIdCategory, findInNormalizedStateIdCategory, getNewNormalizedId, getNormalizedStateIdCategoryIterator, isVariationOf, mapNormalizedStateIdCategory, mergeObjectToState, NormalizedStateIdCategory, setInState, unsetInState} from "../../../../Common/utils/util";
import CTreeEdge from "../Edge/c-tree-edge";
import {CTreeConstants} from "./c-tree-constants";
import {default as KnowledgeBase} from "../../../Denotation/KnowledgeBase/knowledge-base";
import {VocabularySymbol} from "../../../Denotation/Vocabulary/vocabulary-symbol";
import DL_DENOTATION_HELPER_FUNCTIONS from "../../../Denotation/utils/denotation-helper-functions";
import {CTREE_NODE_ORIGIN, default as CTreeNode} from "../Node/c-tree-node";
import DLCTreeSolvableProblemRouter from "../../../Problem/utils/dl-ctree-solvable-problem-router";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import Expression from "../../../Denotation/KnowledgeBase/Expression/expression";
import ExpressionRouter from "../../../Denotation/KnowledgeBase/Expression/utils/expression-router";
import {BottomHelper} from "../../../Denotation/KnowledgeBase/Expression/bottom";
import ConceptRouter, {ConceptTableauRuleNode} from "../../../Denotation/KnowledgeBase/Expression/utils/concept-router";
import {SubsumptionHelper, TRuleNode} from "../../../Denotation/KnowledgeBase/Expression/subsumption";
import {default as Individual, IndividualHelper} from "../../../Denotation/KnowledgeBase/Expression/individual";
import {default as ABoxAssertionRouter} from "../../../Denotation/KnowledgeBase/Expression/utils/abox-assertion-router";

const CTREE_HELPER_FUNCTIONS = {
    addNode<S extends CTreeState> (state: S, cTreeId: number, origin: CTREE_NODE_ORIGIN, x: number, y: number, nameOrIndividualSymbolId?: string | number, nodeLabel?: number[]) {
        const node = {
            id: NaN,
            nameOrIndividualSymbolId: typeof nameOrIndividualSymbolId === "undefined" ?
                CTREE_HELPER_FUNCTIONS.getNewNodeDefaultName(state.DLCTrees.byId[cTreeId], state.DLCTreeNodes, state.DLVocabularySymbols) :
                nameOrIndividualSymbolId,
            origin,
            x,
            y,
            labelWidth: CTreeConstants.NODE.LABEL.WIDTH.DEFAULT,
            labelHeight: CTreeConstants.NODE.LABEL.HEIGHT.DEFAULT,
            labelConcepts: nodeLabel ? [...nodeLabel] : []
        } as CTreeNode;

        state = appendNormalizedToState(state, ["DLCTreeNodes"], node);
        state = appendToStateArray(state, ["DLCTrees", "byId", cTreeId, "nodes"], node.id, false);
        state = CTREE_HELPER_FUNCTIONS.updateCTreeReasoningProcessStage(state, cTreeId);
        return [state, node.id] as [S, number];
    },
    addNodetoCTree<S extends CTreeState> (state: S, cTreeId: number, cTreeNodeId: number) {
        return appendToStateArray(state, ["DLCTrees", "byId", cTreeId, "nodes"], cTreeNodeId, false);
    },
    removeNodeFromCTree<S extends CTreeState> (state: S, cTreeId: number, cTreeNodeId: number) {
        return unsetInState(state, ["DLCTrees", "byId", cTreeId, "nodes", cTreeNodeId]);
    },
    getNewNodeDefaultName<S extends CTreeState> (cTree: CTree, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>) {
        let suffix = 0;
        let available = false;

        const names = mapNormalizedStateIdCategory(filterNormalizedStateIdCategory(cTreeNodes, cTree.nodes), node => CTREE_HELPER_FUNCTIONS.getNodeName(node.nameOrIndividualSymbolId, vocabularySymbols));

        names.push(...mapNormalizedStateIdCategory(vocabularySymbols, symbol => symbol.symbol));

        const namesSet = new Set(names);

        while (!available) {
            available = true;

            if (namesSet.has(`s${suffix}`)) {
                suffix++;
                available = false;
                break;
            }
        }

        return `s${suffix}`;
    },
    getEqualConceptInNodeLabel (cTreeNode: CTreeNode, conceptId: number, expressions: NormalizedStateIdCategory<Expression>) {
        const needle = ExpressionRouter.getNormalizedUniqueString(conceptId, expressions);

        for (const id of cTreeNode.labelConcepts) {
            if (needle === ExpressionRouter.getNormalizedUniqueString(id, expressions)) {
                return id;
            }
        }

        return undefined;
    },
    getNodeCTree (cTreeNodeId: number, cTrees: NormalizedStateIdCategory<CTree>) {
        return findInNormalizedStateIdCategory(cTrees, cTree => cTree.nodes.indexOf(cTreeNodeId) !== -1);
    },
    getTableauCTree (tableauId: number, cTrees: NormalizedStateIdCategory<CTree>) {
        return findInNormalizedStateIdCategory(cTrees, cTree => cTree.tableauId === tableauId);
    },
    connectNodes<S extends CTreeState> (state: S, cTreeId: number, startNodeId: number, endNodeId: number) {
        const filteredCTreeEdges = filterNormalizedStateIdCategory(state.DLCTreeEdges, state.DLCTrees.byId[cTreeId].edges);

        let edge = findInNormalizedStateIdCategory(filteredCTreeEdges,
            edge =>
                edge.startNodeId === startNodeId &&
                edge.endNodeId === endNodeId,
            state.DLCTrees.byId[cTreeId].edges
        );

        if (!edge) {
            edge = {
                id: getNewNormalizedId(state.DLCTreeEdges),
                startNodeId: startNodeId,
                endNodeId: endNodeId,
                labelRoles: []
            } as CTreeEdge;

            state = appendNormalizedToState(state, ["DLCTreeEdges"], edge);
        }
        state = appendToStateArray(state, ["DLCTrees", "byId", cTreeId, "edges"], edge.id, false);

        state = CTREE_HELPER_FUNCTIONS.updateCTreeReasoningProcessStage(state, cTreeId);
        return [state, edge] as [S, CTreeEdge];
    },
    nodeSet<S extends CTreeState> (state: S, nodeId: number, attribute: keyof CTreeNode, newValue: any) {
        return setInState(state, ["DLCTreeNodes", "byId", nodeId, attribute], newValue, true);
    },
    nodeMerge<S extends CTreeState> (state: S, nodeId: number, attributes: Partial<CTreeNode>) {
        return mergeObjectToState(state, ["DLCTreeNodes", "byId", nodeId], attributes);
    },
    extendEdgeLabel<S extends CTreeState> (state: S, cTreeEdgeId: number, roleId: number) {
        return appendToStateArray(state, ["DLCTreeEdges", "byId", cTreeEdgeId, "labelRoles"], roleId, false);
    },
    extendNodeLabel<S extends CTreeState> (state: S, cTreeNodeId: number, conceptId: number | number[]) {
        state = appendToStateArray(state, ["DLCTreeNodes", "byId", cTreeNodeId, "labelConcepts"], conceptId, false);
        state = CTREE_HELPER_FUNCTIONS.updateCTreeReasoningProcessStage(state, CTREE_HELPER_FUNCTIONS.getNodeCTree(cTreeNodeId, state.DLCTrees)!.id);
        return state;
    },
    removeFromNodeLabel<S extends CTreeState> (state: S, cTreeNodeId: number, conceptId: number | number[]) {
        return unsetInState(state, ["DLCTreeNodes", "byId", cTreeNodeId, "labelConcepts", conceptId]);
    },
    getNodeName (nodeNameOrSymbolId: string | number, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>) {
        return typeof nodeNameOrSymbolId === "number" ?
            vocabularySymbols.byId[nodeNameOrSymbolId].symbol :
            nodeNameOrSymbolId;
    },
    getIndividualsToInitializeInCTree (knowledgeBase: KnowledgeBase, expressions: NormalizedStateIdCategory<Expression>, cTreeNodes: NormalizedStateIdCategory<CTreeNode>) {
        const individualIds = DL_DENOTATION_HELPER_FUNCTIONS.getKnowledgeBaseIndividuals(knowledgeBase, expressions);

        for (const node of getNormalizedStateIdCategoryIterator(cTreeNodes)) {
            if (typeof node.nameOrIndividualSymbolId === "number") {
                individualIds.delete(node.nameOrIndividualSymbolId as number);
            }
        }

        return Array.from(individualIds);
    },
    getCTreeContradictions (cTree: CTree, expressions: NormalizedStateIdCategory<Expression>, cTreeNodes: NormalizedStateIdCategory<CTreeNode>) {
        const result: CTreeContradictions = {};
        for (const nodeId of cTree.nodes) {
            const node = cTreeNodes.byId[nodeId];
            for (const conceptId of node.labelConcepts) {
                if (expressions.byId[conceptId].type === BottomHelper.EXPRESSION_TYPE) {
                    if (typeof result[nodeId] === "undefined") {
                        result[nodeId] = [];
                    }
                    result[nodeId].push(conceptId);
                }
                else {
                    const expressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(conceptId, expressions);

                    if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(node, expressionUpdate.id, expressionUpdate.expressions) !== "undefined") {
                        if (typeof result[nodeId] === "undefined") {
                            result[nodeId] = [];
                        }
                        result[nodeId].push(conceptId);
                    }
                }
            }
        }

        return result;
    },
    getCTreeBlockedNodes (cTree: CTree, expressions: NormalizedStateIdCategory<Expression>, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>): CTreeBlockedNodes {
        const nodeIdsToStartIn = [...cTreeNodes.allIds];
        const anonymousSuccessorMap: {[nodeId: number]: number[]} = {};
        const descendantMap: {[nodeId: number]: number[]} = {};
        const conceptNormalizedStringMap: {[conceptId: number]: string} = {};
        const nodeLabelNormalizedConcepts: {[nodeId: number]: string[]} = {};
        const nodesBlockedBy: {[nodeId: number]: number} = {};

        for (const nodeId of cTreeNodes.allIds) {
            anonymousSuccessorMap[nodeId] = [];

            for (const conceptId of cTreeNodes.byId[nodeId].labelConcepts) {
                if (typeof conceptNormalizedStringMap[conceptId] === "undefined") {
                    conceptNormalizedStringMap[conceptId] = ExpressionRouter.getNormalizedUniqueString(conceptId, expressions);
                }
            }
        }

        for (const edge of getNormalizedStateIdCategoryIterator(cTreeEdges)) {
            if (cTreeNodes.byId[edge.endNodeId].origin === CTREE_NODE_ORIGIN.REASONING_PROCESS && typeof cTreeNodes.byId[edge.endNodeId].nameOrIndividualSymbolId === "string") {
                anonymousSuccessorMap[edge.startNodeId].push(edge.endNodeId);
            }
        }

        for (const nodeId of cTreeNodes.allIds) {
            descendantMap[nodeId] = [...anonymousSuccessorMap[nodeId]];
            let descendantIndex = 0;

            while (descendantIndex < descendantMap[nodeId].length) {
                for (const descendantSuccessorId of anonymousSuccessorMap[descendantMap[nodeId][descendantIndex]]) {
                    if (descendantSuccessorId !== nodeId && descendantMap[nodeId].indexOf(descendantSuccessorId) === -1) {
                        descendantMap[nodeId].push(descendantSuccessorId);
                    }
                }
                descendantIndex++;
            }

            nodeLabelNormalizedConcepts[nodeId] = cTreeNodes.byId[nodeId].labelConcepts.map(conceptId => conceptNormalizedStringMap[conceptId]);
        }

        for (const nodeId of nodeIdsToStartIn) {
            for (const descendantId of descendantMap[nodeId]) {
                if (anonymousSuccessorMap[nodeId].indexOf(descendantId) !== -1 ||
                    typeof nodesBlockedBy[descendantId] === "undefined") {


                    if (typeof nodesBlockedBy[nodeId] !== "undefined" || isVariationOf(nodeLabelNormalizedConcepts[descendantId], nodeLabelNormalizedConcepts[nodeId])) {
                        nodesBlockedBy[descendantId] = nodeId;
                    }
                }
            }
        }
        return nodesBlockedBy;
    },
    getEdgeLabelRoleStrings (labelRoles: number[], expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>) {
        return labelRoles.map(id => ExpressionRouter.getSimpleString(id, expressions, vocabularySymbols));
    },
    getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>) {
        const cTreeNode = cTreeNodes.byId[cTreeNodeId];
        return cTreeNode.labelConcepts.reduce((result, conceptId) => {
                result.push(...ConceptRouter.getNewApplicableTableauRuleNodes(cTreeNodeId, conceptId, cTreeNodes, cTreeEdges, expressions));
                return result;
            }, [] as ConceptTableauRuleNode[]
        );
    },
    getNewApplicableTableauTRuleNodes (cTreeNodeId: number, tBoxAxiomIds: number[], cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>) {
        return tBoxAxiomIds.reduce((result, tBoxAxiomId) => {
                result.push(...SubsumptionHelper.getNewApplicableTRuleNodes(cTreeNodeId, tBoxAxiomId, cTreeNodes, cTreeEdges, expressions));
                return result;
            }, [] as TRuleNode[]
        );
    },
    updateCTreeReasoningProcessStage<S extends CTreeState> (state: S, cTreeId: number): S {
        let cTree = state.DLCTrees.byId[cTreeId];
        if (cTree.reasoningProcessStage === CTreeReasoningProcessStage.PROBLEM_INTIALIZATION) {
            if (DLCTreeSolvableProblemRouter.isCTreeProblemInitialized(cTreeId, state)) {
                state = setInState(state, ["DLCTrees", "byId", cTreeId, "reasoningProcessStage"], CTreeReasoningProcessStage.KNOWLEDGE_BASE_INITIALIZATION, true);
            }
        }
        cTree = state.DLCTrees.byId[cTreeId];
        if (cTree.reasoningProcessStage === CTreeReasoningProcessStage.KNOWLEDGE_BASE_INITIALIZATION) {
            const cTree = state.DLCTrees.byId[cTreeId];
            if (CTREE_HELPER_FUNCTIONS.getIndividualsToInitializeInCTree(state.DLKnowledgeBases.byId[cTree.knowledgeBaseId], state.DLExpressions, filterNormalizedStateIdCategory(state.DLCTreeNodes, cTree.nodes)).length === 0 && cTree.nodes.length > 0) {
                state = setInState(state, ["DLCTrees", "byId", cTreeId, "reasoningProcessStage"], CTreeReasoningProcessStage.TABLEAU_RULE_APPLICATION, true);
                state = CTREE_HELPER_FUNCTIONS.applyABoxAssertions(state, cTreeId);
                state = TableauRuleRouter.updateActiveTableauNodeState(cTree.tableauId, state);

                state = CTREE_HELPER_FUNCTIONS.setCTreeStatus(state, cTreeId, "The completion tree has been succesfully initialized. Now you can continue the reasoning process by applying tableau rules.", "positive");
            }
        }
        return state;
    },
    setCTreeStatus<S extends CTreeState> (state: S, cTreeId: number, statusMessage: string, statusType: CTreeStatusBarStatusType): S {
        state = setInState(state, ["DLCTrees", "byId", cTreeId, "statusMessage"], statusMessage, true);
        state = setInState(state, ["DLCTrees", "byId", cTreeId, "statusType"], statusType, true);
        return state;
    },
    applyABoxAssertions<S extends CTreeState> (state: S, cTreeId: number) {
        const cTree = state.DLCTrees.byId[cTreeId];
        const knowledgeBase = state.DLKnowledgeBases.byId[cTree.knowledgeBaseId];

        const initializedIndividualIds = mapNormalizedStateIdCategory(filterNormalizedStateIdCategory(state.DLCTreeNodes, node => typeof node.nameOrIndividualSymbolId === "number" && cTree.nodes.indexOf(node.id) !== -1), node => node.nameOrIndividualSymbolId);

        axiomLoop:
            for (const aId of knowledgeBase.aBoxAxiomIds) {
                const relatedIndividualIds = ExpressionRouter.getRelatedExpressionIds(aId, state.DLExpressions)
                    .filter(rId => state.DLExpressions.byId[rId].type === IndividualHelper.EXPRESSION_TYPE)
                    .map(rId => (state.DLExpressions.byId[rId] as Individual).symbolId);

                for (const rIId of relatedIndividualIds) {
                    if (initializedIndividualIds.indexOf(rIId) === -1) {
                        continue axiomLoop;
                    }
                }
                state = ABoxAssertionRouter.applyAssertionOnCTree(aId, cTreeId, state);
            }

        return state;
    }
};

export interface CTreeContradictions {
    [nodeId: number]: number[]
}

export interface CTreeBlockedNodes {
    [nodeId: number]: number
}

export default CTREE_HELPER_FUNCTIONS;