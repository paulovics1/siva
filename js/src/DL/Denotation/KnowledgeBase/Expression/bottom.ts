import Expression, {ExpressionCategory} from "./expression";
import {ConceptTableauRuleNode, default as ConceptRouter, TableauNodeType} from "./utils/concept-router";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import {TopHelper} from "./top";
import {NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";

export default interface Bottom extends Expression {}

export class BottomHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.NONE;
    static readonly EXPRESSION_TYPE = "Bottom";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.VOCABULARY_SYMBOL;
    static readonly SPECIAL_SYMBOLS = {
        BOTTOM: {
            symbol: "\u22A5",
            latex: "\\bot"
        }
    };

    static create (id?: number) {
        return {
            id: id,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Bottom;
    }

    static pushRelatedExpressionIdsToArray (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        pushToArrayIfAbsent(bottom.id, accumulator);
    }

    static getLatexString (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return this.SPECIAL_SYMBOLS.BOTTOM.latex;
    }

    static getSimpleString (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return this.SPECIAL_SYMBOLS.BOTTOM.symbol;
    }

    static getUniqueString (bottom: Bottom): string {
        return `${this.EXPRESSION_TYPE}`;
    }

    static getNormalizedUniqueString (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        if (expression === this.SPECIAL_SYMBOLS.BOTTOM.symbol) {
            const parsedExpression = this.create();
            return ExpressionRouter.getExpressionUpdate(parsedExpression, expressions);
        }

        return null;
    }

    static getNNFExpressionUpdate (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return {
            id: bottom.id,
            expressions: expressions
        };
    }

    static getNegatedNNFExpressionUpdate (bottom: Bottom, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        return ExpressionRouter.getExpressionUpdate(TopHelper.create(), expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): ConceptTableauRuleNode[] {
        return [];
    }

    static getConceptTableauRuleDescription (bottom: Bottom, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `No tableau rule is applicable to ${this.getSimpleString(bottom, expressions, vocabularySymbols)}.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
    }
}

ExpressionRouter.registerHelper(BottomHelper);
ConceptRouter.registerHelper(BottomHelper);
