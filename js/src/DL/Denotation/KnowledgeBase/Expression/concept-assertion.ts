import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ABoxAssertionRouter from "./utils/abox-assertion-router";
import Individual from "./individual";
import {filterNormalizedStateIdCategory, findFirstGlobalCharOccurence, findInNormalizedStateIdCategory, NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import {CTreeState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";

export default interface ConceptAssertion extends Expression {
    individualId: number,
    conceptId: number
}

export class ConceptAssertionHelper {
    static readonly EXPRESSION_TYPE = "ConceptAssertion";
    static readonly CATEGORY = ExpressionCategory.ABOX_AXIOM;
    static readonly PRECEDENCE = ExpressionPrecedence.AXIOM;

    static create (individualId: number, conceptId: number, id?: number) {
        return {
            id: id,
            individualId,
            conceptId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as ConceptAssertion;
    }

    static pushRelatedExpressionIdsToArray (conceptAssertion: ConceptAssertion, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(conceptAssertion.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(conceptAssertion.individualId, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(conceptAssertion.conceptId, expressions, accumulator);
        }
    }

    static getLatexString (conceptAssertion: ConceptAssertion, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getLatexString(conceptAssertion.individualId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} : ${ExpressionRouter.getLatexString(conceptAssertion.conceptId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (conceptAssertion: ConceptAssertion, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getSimpleString(conceptAssertion.individualId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} : ${ExpressionRouter.getSimpleString(conceptAssertion.conceptId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (conceptAssertion: ConceptAssertion): string {
        return `${this.EXPRESSION_TYPE}(${conceptAssertion.individualId}, ${conceptAssertion.conceptId})`;
    }

    static getNormalizedUniqueString (conceptAssertion: ConceptAssertion, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(conceptAssertion.individualId, expressions)},${ExpressionRouter.getNormalizedUniqueString(conceptAssertion.conceptId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        const colonIndex = findFirstGlobalCharOccurence(":", expression);
        const commaIndex = findFirstGlobalCharOccurence(",", expression);

        if (colonIndex !== -1 && commaIndex === -1) {
            const parsedIndividual = ExpressionRouter.parseString(expression.substr(0, colonIndex), expressions, vocabularySymbols, vocabulary, ExpressionCategory.INDIVIDUAL);
            const parsedConcept = ExpressionRouter.parseString(expression.substr(colonIndex + 1), parsedIndividual.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedIndividual.id, parsedConcept.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedConcept.expressions);
        }
        return null;
    }

    static applyAssertionOnCTree<S extends CTreeState> (assertionId: number, cTreeId: number, state: S): S {
        const conceptAssertion = state.DLExpressions.byId[assertionId] as ConceptAssertion;
        const individual = state.DLExpressions.byId[conceptAssertion.individualId] as Individual;
        const cTree = state.DLCTrees.byId[cTreeId];
        const filteredCTreeNodes = filterNormalizedStateIdCategory(state.DLCTreeNodes, cTree.nodes);

        const cTreeNode = findInNormalizedStateIdCategory(filteredCTreeNodes, node => node.nameOrIndividualSymbolId === individual.symbolId)!;

        return CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, cTreeNode.id, conceptAssertion.conceptId);
    }
}

ExpressionRouter.registerHelper(ConceptAssertionHelper);
ABoxAssertionRouter.registerHelper(ConceptAssertionHelper);