import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ConceptRouter, {ConceptTableauRuleNode, TableauNodeType} from "./utils/concept-router";
import {UnionHelper} from "./union";
import {findFirstGlobalCharOccurence, NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import {TableauState} from "../../../../Tableau/tableau";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import CTreeView from "../../../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../../../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";
import {CTreeState, CTreeTableauNodeDescriptionState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";

export default interface Intersection extends Expression {
    concept1Id: number,
    concept2Id: number
}

export class IntersectionHelper {
    static readonly TABLEAU_NODE_TYPE = TableauNodeType.AND;
    static readonly TABLEAU_ACTION_TYPE = "CTreeTableauIntersectionRule";
    static readonly EXPRESSION_TYPE = "Intersection";
    static readonly CATEGORY = ExpressionCategory.CONCEPT;
    static readonly PRECEDENCE = ExpressionPrecedence.BINARY_LOW;
    static readonly SPECIAL_SYMBOLS = {
        INTERSECTION: {
            symbol: "\u2293",
            latex: "\\sqcap"
        }
    };

    static create (concept1Id: number, concept2Id: number, id?: number) {
        return {
            id: id,
            concept1Id,
            concept2Id,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Intersection;
    }

    static pushRelatedExpressionIdsToArray (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(intersection.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(intersection.concept1Id, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(intersection.concept2Id, expressions, accumulator);
        }
    }

    static getLatexString (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getLatexString(intersection.concept1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.INTERSECTION.latex} ${ExpressionRouter.getLatexString(intersection.concept2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getSimpleString(intersection.concept1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.INTERSECTION.symbol} ${ExpressionRouter.getSimpleString(intersection.concept2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (intersection: Intersection): string {
        return `${this.EXPRESSION_TYPE}(${intersection.concept1Id}, ${intersection.concept2Id})`;
    }

    static getNormalizedUniqueString (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>): string {
        const sortedIds = [intersection.concept1Id, intersection.concept2Id].sort();
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(sortedIds[0], expressions)},${ExpressionRouter.getNormalizedUniqueString(sortedIds[1], expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        const index = findFirstGlobalCharOccurence(this.SPECIAL_SYMBOLS.INTERSECTION.symbol, expression);

        if (index !== -1) {
            const parsedConcept1 = ExpressionRouter.parseString(expression.substr(0, index), expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);
            const parsedConcept2 = ExpressionRouter.parseString(expression.substr(index + 1), parsedConcept1.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedConcept1.id, parsedConcept2.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedConcept2.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const nnfConcept1ExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(intersection.concept1Id, expressions);
        const nnfConcept2ExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(intersection.concept2Id, nnfConcept1ExpressionUpdate.expressions);

        return ExpressionRouter.getExpressionUpdate(this.create(nnfConcept1ExpressionUpdate.id, nnfConcept2ExpressionUpdate.id), nnfConcept2ExpressionUpdate.expressions);
    }

    static getNegatedNNFExpressionUpdate (intersection: Intersection, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const negNnfConcept1ExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(intersection.concept1Id, expressions);
        const negNnfConcept2ExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(intersection.concept2Id, negNnfConcept1ExpressionUpdate.expressions);

        return ExpressionRouter.getExpressionUpdate(UnionHelper.create(negNnfConcept1ExpressionUpdate.id, negNnfConcept2ExpressionUpdate.id), negNnfConcept2ExpressionUpdate.expressions);
    }

    static getNewApplicableCTreeTableauConceptRuleNodes (cTreeNodeId: number, conceptId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): IntersectionTableauRuleNode[] {
        const intersection = expressions.byId[conceptId] as Intersection;
        const newConceptIds = [];

        if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeNodeId], intersection.concept1Id, expressions) === "undefined") {
            newConceptIds.push(intersection.concept1Id);
        }
        if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeNodeId], intersection.concept2Id, expressions) === "undefined") {
            if (newConceptIds.indexOf(intersection.concept2Id) === -1) {
                newConceptIds.push(intersection.concept2Id);
            }
        }

        if (newConceptIds.length > 0) {
            return [{
                id: NaN,
                actionType: this.TABLEAU_ACTION_TYPE,
                childrenIds: [],
                state: undefined,
                conceptId: intersection.id,
                cTreeNodeId: cTreeNodeId,
                newConceptIds: newConceptIds
            }];
        }

        return [];
    }

    static getConceptTableauRuleDescription (intersection: Intersection, cTreeNode: CTreeNode, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        const concept1String = ExpressionRouter.getSimpleString(intersection.concept1Id, expressions, vocabularySymbols);
        const concept2String = ExpressionRouter.getSimpleString(intersection.concept2Id, expressions, vocabularySymbols);
        return `Insert "${concept1String}" and "${concept2String}" into the current node's label.`;
    }

    static onConceptTableauExpand (cTreeNodeId: number, conceptId: number, cTreeView: CTreeView): void {
        cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_APPLY_CONCEPT_TABLEAU_AND_RULE(cTreeNodeId, conceptId));
    }

    static applyTableauRule<S extends TableauState> (intersectionTableauRuleNode: IntersectionTableauRuleNode, state: S & CTreeState): S {
        state = CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, intersectionTableauRuleNode.cTreeNodeId, intersectionTableauRuleNode.newConceptIds);
        return state;
    }

    static revertTableauRule<S extends TableauState> (intersectionTableauRuleNode: IntersectionTableauRuleNode, state: S & CTreeState): S {
        state = CTREE_HELPER_FUNCTIONS.removeFromNodeLabel(state, intersectionTableauRuleNode.cTreeNodeId, intersectionTableauRuleNode.newConceptIds);
        return state;
    }

    static getTableauNodeLabelText (intersectionTableauRuleNode: IntersectionTableauRuleNode, tableauNodeDescriptionState: CTreeTableauNodeDescriptionState): string {
        const cTreeNodeName = tableauNodeDescriptionState.DLCTreeNodes.byId[intersectionTableauRuleNode.cTreeNodeId] ?
            CTREE_HELPER_FUNCTIONS.getNodeName(tableauNodeDescriptionState.DLCTreeNodes.byId[intersectionTableauRuleNode.cTreeNodeId].nameOrIndividualSymbolId, tableauNodeDescriptionState.DLVocabularySymbols) :
            "";

        const conceptString = ExpressionRouter.getSimpleString(intersectionTableauRuleNode.conceptId, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);

        return `<b>\u2022 ${cTreeNodeName}</b>\n${this.SPECIAL_SYMBOLS.INTERSECTION.symbol}-rule`;
    }
}

interface IntersectionTableauRuleNode extends ConceptTableauRuleNode {
    newConceptIds: number[]
}

ExpressionRouter.registerHelper(IntersectionHelper);
ConceptRouter.registerHelper(IntersectionHelper);
TableauRuleRouter.registerHelper(IntersectionHelper);