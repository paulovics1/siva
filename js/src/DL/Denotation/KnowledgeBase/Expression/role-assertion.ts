import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ABoxAssertionRouter from "./utils/abox-assertion-router";
import Individual from "./individual";
import {filterNormalizedStateIdCategory, findFirstGlobalCharOccurence, findInNormalizedStateIdCategory, NormalizedStateIdCategory, pushToArrayIfAbsent} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import {CTreeState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";

export default interface RoleAssertion extends Expression {
    individual1Id: number,
    individual2Id: number,
    roleId: number
}

export class RoleAssertionHelper {
    static readonly EXPRESSION_TYPE = "RoleAssertion";
    static readonly CATEGORY = ExpressionCategory.ABOX_AXIOM;
    static readonly PRECEDENCE = ExpressionPrecedence.AXIOM;

    static create (individual1Id: number, individual2Id: number, roleId: number, id?: number) {
        return {
            id: id,
            individual1Id,
            individual2Id,
            roleId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as RoleAssertion;
    }

    static pushRelatedExpressionIdsToArray (roleAssertion: RoleAssertion, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(roleAssertion.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(roleAssertion.individual1Id, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(roleAssertion.individual2Id, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(roleAssertion.roleId, expressions, accumulator);
        }
    }

    static getLatexString (roleAssertion: RoleAssertion, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getLatexString(roleAssertion.individual1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)},${ExpressionRouter.getLatexString(roleAssertion.individual2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} : ${ExpressionRouter.getLatexString(roleAssertion.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (roleAssertion: RoleAssertion, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getSimpleString(roleAssertion.individual1Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}, ${ExpressionRouter.getSimpleString(roleAssertion.individual2Id, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} : ${ExpressionRouter.getSimpleString(roleAssertion.roleId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (roleAssertion: RoleAssertion): string {
        return `${this.EXPRESSION_TYPE}(${roleAssertion.individual1Id}, ${roleAssertion.individual2Id}, ${roleAssertion.roleId})`;
    }

    static getNormalizedUniqueString (roleAssertion: RoleAssertion, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(roleAssertion.individual1Id, expressions)},${ExpressionRouter.getNormalizedUniqueString(roleAssertion.individual2Id, expressions)},${ExpressionRouter.getNormalizedUniqueString(roleAssertion.roleId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        const commaIndex = findFirstGlobalCharOccurence(",", expression);
        const colonIndex = findFirstGlobalCharOccurence(":", expression, commaIndex);

        if (colonIndex !== -1 && commaIndex !== -1) {
            const parsedIndividual1 = ExpressionRouter.parseString(expression.substr(0, commaIndex), expressions, vocabularySymbols, vocabulary, ExpressionCategory.INDIVIDUAL);
            const parsedIndividual2 = ExpressionRouter.parseString(expression.substr(commaIndex + 1, colonIndex - (commaIndex + 1)), parsedIndividual1.expressions, vocabularySymbols, vocabulary, ExpressionCategory.INDIVIDUAL);
            const parsedRole = ExpressionRouter.parseString(expression.substr(colonIndex + 1), parsedIndividual2.expressions, vocabularySymbols, vocabulary, ExpressionCategory.ROLE);

            const parsedExpression = this.create(parsedIndividual1.id, parsedIndividual2.id, parsedRole.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedRole.expressions);
        }
        return null;
    }

    static applyAssertionOnCTree<S extends CTreeState> (assertionId: number, cTreeId: number, state: S): S {
        const roleAssertion = state.DLExpressions.byId[assertionId] as RoleAssertion;
        const individual1 = state.DLExpressions.byId[roleAssertion.individual1Id] as Individual;
        const individual2 = state.DLExpressions.byId[roleAssertion.individual2Id] as Individual;

        const filteredCTreeNodes = filterNormalizedStateIdCategory(state.DLCTreeNodes, state.DLCTrees.byId[cTreeId].nodes);

        const cTreeNode1 = findInNormalizedStateIdCategory(filteredCTreeNodes, node => node.nameOrIndividualSymbolId === individual1.symbolId)!;
        const cTreeNode2 = findInNormalizedStateIdCategory(filteredCTreeNodes, node => node.nameOrIndividualSymbolId === individual2.symbolId)!;

        let cTreeEdge;

        [state, cTreeEdge] = CTREE_HELPER_FUNCTIONS.connectNodes(state, cTreeId, cTreeNode1.id, cTreeNode2.id);

        return CTREE_HELPER_FUNCTIONS.extendEdgeLabel(state, cTreeEdge.id, roleAssertion.roleId);
    }
}

ExpressionRouter.registerHelper(RoleAssertionHelper);
ABoxAssertionRouter.registerHelper(RoleAssertionHelper);