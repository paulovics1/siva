import Expression, {ExpressionCategory} from "./expression";
import {default as ExpressionRouter, ExpressionPrecedence, ExpressionUpdate} from "./utils/expression-router";
import ConceptRouter, {CTreeNodeTableauRuleNode} from "./utils/concept-router";
import {UnionHelper} from "./union";
import {findFirstGlobalCharOccurence, NormalizedStateIdCategory, pushToArrayIfAbsent, removeFromArray, setInState} from "../../../../Common/utils/util";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Vocabulary/vocabulary";
import {TableauState} from "../../../../Tableau/tableau";
import TableauNode from "../../../../Tableau/tableau-node";
import TableauRuleRouter from "../../../../Tableau/utils/tableau-rule-router";
import CTreeNode from "../../../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeEdge from "../../../CTreeTableauReasoning/CompletionTree/Edge/c-tree-edge";
import CTREE_HELPER_FUNCTIONS from "../../../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import {CTreeState, CTreeTableauNodeDescriptionState} from "../../../CTreeTableauReasoning/CompletionTree/c-tree";

export default interface Subsumption extends Expression {
    subsumeeId: number,
    subsumerId: number
}

export class SubsumptionHelper {
    static readonly EXPRESSION_TYPE = "Subsumption";
    static readonly TABLEAU_ACTION_TYPE = "TableauTRule";
    static readonly CATEGORY = ExpressionCategory.TBOX_AXIOM;
    static readonly PRECEDENCE = ExpressionPrecedence.AXIOM;
    static readonly SPECIAL_SYMBOLS = {
        SUBSUMPTION: {
            symbol: "\u2291",
            latex: "\\sqsubseteq"
        }
    };

    static create (subsumeeId: number, subsumerId: number, id?: number) {
        return {
            id: id,
            subsumeeId,
            subsumerId,
            category: this.CATEGORY,
            type: this.EXPRESSION_TYPE
        } as Subsumption;
    }

    static pushRelatedExpressionIdsToArray (subsumption: Subsumption, expressions: NormalizedStateIdCategory<Expression>, accumulator: number[]): void {
        if (pushToArrayIfAbsent(subsumption.id, accumulator)) {
            ExpressionRouter.pushRelatedExpressionIdsToArray(subsumption.subsumeeId, expressions, accumulator);
            ExpressionRouter.pushRelatedExpressionIdsToArray(subsumption.subsumerId, expressions, accumulator);
        }
    }

    static getLatexString (subsumption: Subsumption, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getLatexString(subsumption.subsumeeId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.SUBSUMPTION.latex} ${ExpressionRouter.getLatexString(subsumption.subsumerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getSimpleString (subsumption: Subsumption, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>): string {
        return `${ExpressionRouter.getSimpleString(subsumption.subsumeeId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)} ${this.SPECIAL_SYMBOLS.SUBSUMPTION.symbol} ${ExpressionRouter.getSimpleString(subsumption.subsumerId, expressions, vocabularySymbols, this.EXPRESSION_TYPE)}`;
    }

    static getUniqueString (subsumption: Subsumption): string {
        return `${this.EXPRESSION_TYPE}(${subsumption.subsumeeId}, ${subsumption.subsumerId})`;
    }

    static getNormalizedUniqueString (subsumption: Subsumption, expressions: NormalizedStateIdCategory<Expression>): string {
        return `${this.EXPRESSION_TYPE}(${ExpressionRouter.getNormalizedUniqueString(subsumption.subsumeeId, expressions)},${ExpressionRouter.getNormalizedUniqueString(subsumption.subsumerId, expressions)})`;
    }

    static parseString (expression: string, expressions: NormalizedStateIdCategory<Expression>, vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>, vocabulary: Vocabulary): ExpressionUpdate | null {
        const index = findFirstGlobalCharOccurence(this.SPECIAL_SYMBOLS.SUBSUMPTION.symbol, expression);

        if (index !== -1) {
            const parsedSubsumee = ExpressionRouter.parseString(expression.substr(0, index), expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);
            const parsedSubsumer = ExpressionRouter.parseString(expression.substr(index + 1), parsedSubsumee.expressions, vocabularySymbols, vocabulary, ExpressionCategory.CONCEPT);

            const parsedExpression = this.create(parsedSubsumee.id, parsedSubsumer.id);

            return ExpressionRouter.getExpressionUpdate(parsedExpression, parsedSubsumer.expressions);
        }
        return null;
    }

    static getNNFExpressionUpdate (subsumption: Subsumption, expressions: NormalizedStateIdCategory<Expression>): ExpressionUpdate {
        const negNnfSubsumeeExpressionUpdate = ConceptRouter.getNegatedNNFExpressionUpdate(subsumption.subsumeeId, expressions);
        const nnfSubsumerExpressionUpdate = ConceptRouter.getNNFExpressionUpdate(subsumption.subsumerId, negNnfSubsumeeExpressionUpdate.expressions);

        return ExpressionRouter.getExpressionUpdate(UnionHelper.create(negNnfSubsumeeExpressionUpdate.id, nnfSubsumerExpressionUpdate.id), nnfSubsumerExpressionUpdate.expressions);
    }

    static getNewApplicableTRuleNodes (cTreeNodeId: number, subsumptionId: number, cTreeNodes: NormalizedStateIdCategory<CTreeNode>, cTreeEdges: NormalizedStateIdCategory<CTreeEdge>, expressions: NormalizedStateIdCategory<Expression>): TRuleNode[] {
        const expressionUpdate = this.getNNFExpressionUpdate(expressions.byId[subsumptionId] as Subsumption, expressions);

        const result = [];

        if (typeof CTREE_HELPER_FUNCTIONS.getEqualConceptInNodeLabel(cTreeNodes.byId[cTreeNodeId], expressionUpdate.id, expressionUpdate.expressions) === "undefined") {
            result.push({
                id: NaN,
                actionType: this.TABLEAU_ACTION_TYPE,
                childrenIds: [],
                state: undefined,
                cTreeNodeId: cTreeNodeId,
                axiomId: subsumptionId
            });
        }

        return result;
    }

    static applyTableauRule<S extends TableauState> (tRuleNode: TRuleNode, state: S & CTreeState): S {
        const update = this.getNNFExpressionUpdate(state.DLExpressions.byId[tRuleNode.axiomId] as Subsumption, state.DLExpressions);
        state = setInState(state, ["DLExpressions"], update.expressions, true);

        return CTREE_HELPER_FUNCTIONS.extendNodeLabel(state, tRuleNode.cTreeNodeId, update.id);
    }

    static revertTableauRule<S extends TableauState> (tRuleNode: TRuleNode, state: S & CTreeState): S {
        const update = this.getNNFExpressionUpdate(state.DLExpressions.byId[tRuleNode.axiomId] as Subsumption, state.DLExpressions);

        return CTREE_HELPER_FUNCTIONS.removeFromNodeLabel(state, tRuleNode.cTreeNodeId, update.id);
    }

    static getTableauNodeLabelText (tRuleNode: TRuleNode, tableauNodeDescriptionState: CTreeTableauNodeDescriptionState): string {
        const cTreeNodeName = tableauNodeDescriptionState.DLCTreeNodes.byId[tRuleNode.cTreeNodeId] ? CTREE_HELPER_FUNCTIONS.getNodeName(tableauNodeDescriptionState.DLCTreeNodes.byId[tRuleNode.cTreeNodeId].nameOrIndividualSymbolId, tableauNodeDescriptionState.DLVocabularySymbols) : "";

        const conceptString = ExpressionRouter.getSimpleString(tRuleNode.axiomId, tableauNodeDescriptionState.DLExpressions, tableauNodeDescriptionState.DLVocabularySymbols);

        return `<b>\u2022 ${cTreeNodeName}</b>\nT-rule`;
    }

    static doActiveTableauNodeChildrenMatchTRules (axiomIds: number[], cTreeNodeId: number, activeTableauNodeId: number, tableauNodes: NormalizedStateIdCategory<TableauNode>) {
        const unvisitedAxiomIds = [...axiomIds];

        let currentTableauNode = tableauNodes.byId[activeTableauNodeId];

        while (unvisitedAxiomIds.length > 0) {
            if (currentTableauNode.childrenIds.length === 0) {
                return true;
            }

            currentTableauNode = tableauNodes.byId[currentTableauNode.childrenIds[0]];

            if (currentTableauNode.actionType === this.TABLEAU_ACTION_TYPE &&
                (currentTableauNode as TRuleNode).cTreeNodeId === cTreeNodeId &&
                unvisitedAxiomIds.indexOf((currentTableauNode as TRuleNode).axiomId) !== -1) {
                removeFromArray((currentTableauNode as TRuleNode).axiomId, unvisitedAxiomIds);
            }
            else {
                return false;
            }
        }

        return true;
    }
}

export interface TRuleNode extends CTreeNodeTableauRuleNode {
    axiomId: number
}

ExpressionRouter.registerHelper(SubsumptionHelper);
TableauRuleRouter.registerHelper(SubsumptionHelper);