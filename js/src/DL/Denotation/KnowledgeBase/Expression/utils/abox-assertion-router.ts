import {ExpressionHelper} from "./expression-router";
import {CTreeState} from "../../../../CTreeTableauReasoning/CompletionTree/c-tree";

export default class ABoxAssertionRouter {
    private static helpers: Map<string, ExpressionHelper & ABoxAssertionHelper> = new Map();

    static registerHelper (helper: ExpressionHelper & ABoxAssertionHelper) {
        this.helpers.set(helper.EXPRESSION_TYPE, helper);
    }

    static getHelper (conceptType: string) {
        return this.helpers.get(conceptType);
    }

    static applyAssertionOnCTree<S extends CTreeState> (assertionId: number, cTreeId: number, state: S): S {
        const assertion = state.DLExpressions.byId[assertionId];
        const helper = this.getHelper(assertion.type)!;

        return helper.applyAssertionOnCTree(assertionId, cTreeId, state);
    }
}

export interface ABoxAssertionHelper {
    applyAssertionOnCTree<S extends CTreeState> (assertionId: number, cTreeId: number, state: S): S;
}