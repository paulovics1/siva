import * as React from "react";
import {ExpressionCategory} from "../Expression/expression";
import {AxiomBoxPropertiesType} from "../knowledge-base";
import autobind from "autobind-decorator";
import {Dispatch} from "redux";
import MainState from "../../../../App/flux/main-state";
import ExpressionEditControl from "../Expression/components/expression-edit-control";
import KnowledgeBaseView from "./knowledge-base-view";
import KNOWLEDGE_BASE_ACTIONS from "../flux/knowledge-base-actions";

export interface AxiomEditControlProps {
    id: string,
    label?: string
    axiomId?: number | undefined
    initialValue?: string,
    knowledgeBaseId: number,
    axiomCategory: keyof AxiomBoxPropertiesType,
    expectedExpressionCategory: ExpressionCategory,
    setActiveAxiomEditControl: typeof KnowledgeBaseView.prototype.setActiveAxiomEditControl
    toggleActiveEditControlById: typeof KnowledgeBaseView.prototype.toggleActiveEditControlById
    dispatch: Dispatch<MainState>
}

export interface AxiomEditControlState {
}

export default class AxiomEditControl extends React.PureComponent<AxiomEditControlProps, AxiomEditControlState> {
    refs: {
        expressionEditControl: ExpressionEditControl
    };

    constructor (props: AxiomEditControlProps) {
        super(props);

        this.state = {
            axiom: "",
            axiomId: null
        };
    }

    @autobind
    handleConfirm () {
        this.props.dispatch(KNOWLEDGE_BASE_ACTIONS.CREATE_SAVE_AXIOM(this.props.knowledgeBaseId, this.props.axiomCategory, this.refs.expressionEditControl.input.value, this.props.axiomId));
        this.props.toggleActiveEditControlById(this.props.id);
    }

    @autobind
    handleCancel () {
        this.props.toggleActiveEditControlById(this.props.id);
    }

    @autobind
    handleFocus () {
        this.props.setActiveAxiomEditControl(this);
    }

    @autobind
    handleMount (expressionEditControl: ExpressionEditControl) {
        expressionEditControl.input.focus();
        expressionEditControl.input.select();
    }

    render () {
        return (
            <ExpressionEditControl
                ref="expressionEditControl"
                initialValue={this.props.initialValue}
                label={this.props.label}
                onCancel={this.handleCancel}
                onConfirm={this.handleConfirm}
                onFocus={this.handleFocus}
                onMount={this.handleMount}
                expectedExpressionCategory={this.props.expectedExpressionCategory}
            />
        );
    }
}