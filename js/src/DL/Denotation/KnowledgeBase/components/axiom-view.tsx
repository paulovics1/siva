import * as React from "react";
import {MouseEvent} from "react";
import Latex from "react-latex";
import Expression, {ExpressionCategory} from "../Expression/expression";
import Vocabulary from "../../Vocabulary/vocabulary";
import AxiomEditControl from "./axiom-edit-control";
import {AxiomBoxProperties, AxiomBoxPropertiesType} from "../knowledge-base";
import MainState from "../../../../App/flux/main-state";
import {Dispatch} from "redux";
import DL_DENOTATION_HELPER_FUNCTIONS from "../../utils/denotation-helper-functions";
import {NormalizedStateIdCategory} from "../../../../Common/utils/util";
import ExpressionRouter from "../Expression/utils/expression-router";
import {VocabularySymbol} from "../../Vocabulary/vocabulary-symbol";
import ErrorExpression from "../Expression/error-expression";
import KnowledgeBaseView from "./knowledge-base-view";
import autobind from "autobind-decorator";
import ConfirmationModal from "../../../../Common/components/confirmation-modal";
import {MainComponent} from "../../../../App/components/main-component";
import KNOWLEDGE_BASE_ACTIONS from "../flux/knowledge-base-actions";

export interface AxiomViewProps {
    knowledgeBaseId: number,
    axiomCategory: keyof AxiomBoxPropertiesType,
    axiomIndex: number,
    axiomId: number,
    expressions: NormalizedStateIdCategory<Expression>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    vocabulary: Vocabulary,
    expectedExpressionCategory: ExpressionCategory,
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    dispatch: Dispatch<MainState>
    toggleActiveEditControlById: typeof KnowledgeBaseView.prototype.toggleActiveEditControlById
    setActiveAxiomEditControl: typeof KnowledgeBaseView.prototype.setActiveAxiomEditControl
    activeAxiomEditControlId: string | undefined
    editable: boolean
}

export interface AxiomViewState {
    errorIds: number[];
}

export default class AxiomView extends React.PureComponent<AxiomViewProps, AxiomViewState> {
    constructor (props: AxiomViewProps) {
        super(props);

        this.state = {
            errorIds: []
        };
    }

    getAxiomLabelPrefix () {
        return AxiomBoxProperties[this.props.axiomCategory].LABEL.charAt(0);
    }

    @autobind
    handleClick () {
        if (this.props.editable && this.getAxiomLabel() !== this.props.activeAxiomEditControlId) {
            this.props.toggleActiveEditControlById(this.getAxiomLabel());
        }
    }

    getAxiomLabel () {
        return `${this.getAxiomLabelPrefix()}${this.props.axiomIndex + 1}`;
    }

    getClassName () {
        const result = ["axiom-box__axiom-view"];

        if (this.state.errorIds.length !== 0) {
            result.push("row-error");
        }

        return result.join(" ");
    }

    updateErrorIds () {
        this.setState({
            errorIds: DL_DENOTATION_HELPER_FUNCTIONS.checkExpressionForErrors(this.props.axiomId, this.props.expressions)
        });
    }

    componentWillMount () {
        this.updateErrorIds();
    }

    componentWillReceiveProps () {
        this.updateErrorIds();
    }


    @autobind
    handleRemoveClick (e: MouseEvent<HTMLElement>) {
        e.stopPropagation();


        this.props.fillAndShowModal(ConfirmationModal, {
            title: "Vocabulary symbol deletion confirmation",
            message: `Are you sure you want to delete the following axiom?\n${ExpressionRouter.getSimpleString(this.props.axiomId, this.props.expressions, this.props.vocabularySymbols)}`,
            onConfirm: () => {
                this.props.dispatch(KNOWLEDGE_BASE_ACTIONS.CREATE_DELETE_AXIOM(this.props.knowledgeBaseId, this.props.axiomId));
            }
        });
    }

    render () {
        const label = this.getAxiomLabel();
        const editing = this.props.editable && label === this.props.activeAxiomEditControlId;

        const simpleString = ExpressionRouter.getSimpleString(this.props.axiomId, this.props.expressions, this.props.vocabularySymbols);

        let title = simpleString;

        this.state.errorIds.forEach(id => {
            title += `\n${(this.props.expressions.byId[id] as ErrorExpression).errorMessage}`;
        });

        return (
            <tr
                className={this.getClassName()}
                onClick={this.handleClick}
                title={title}
            >
                {
                    editing ?
                        (<td
                            colSpan={2}
                        >
                            <AxiomEditControl
                                id={label}
                                axiomId={this.props.axiomId}
                                initialValue={simpleString}
                                label={"Edit axiom:"}
                                knowledgeBaseId={this.props.knowledgeBaseId}
                                axiomCategory={this.props.axiomCategory}
                                expectedExpressionCategory={this.props.expectedExpressionCategory}
                                dispatch={this.props.dispatch}
                                setActiveAxiomEditControl={this.props.setActiveAxiomEditControl}
                                toggleActiveEditControlById={this.props.toggleActiveEditControlById}
                            />
                        </td>) :
                        ([
                            <td
                                key="0"
                                className={"latex-ellipsis"}
                            >
                                <Latex
                                    children={`$${ExpressionRouter.getLatexString(this.props.axiomId, this.props.expressions, this.props.vocabularySymbols)}$`}
                                />
                            </td>,
                            <td
                                key="1"
                            >
                                {
                                    !this.props.editable || <span
                                        className="remove-icon glyphicon glyphicon-trash"
                                        onClick={this.handleRemoveClick}
                                        title={`Remove ${simpleString}`}
                                    />
                                }
                                <Latex
                                    children={`$\\mathcal{${this.getAxiomLabelPrefix()}${this.props.axiomIndex + 1}}$`}
                                />
                            </td>
                        ])
                }
            </tr>
        );
    }
}