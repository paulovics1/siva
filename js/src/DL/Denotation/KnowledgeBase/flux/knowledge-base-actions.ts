import {AnyAction} from "../../../../App/flux/actions";
import {ExpressionCategory} from "../Expression/expression";
import {VocabularyAlphabet} from "../../Vocabulary/vocabulary";
import {WorkspaceAction} from "../../../../Workspace/workspace";
import {AxiomBoxPropertiesType} from "../knowledge-base";

export namespace KNOWLEDGE_BASE_ACTIONS {
    export const PARSE_AND_SAVE_EXPRESSION = "PARSE_AND_SAVE_EXPRESSION";
    export const SAVE_AXIOM = "SAVE_AXIOM";
    export const DELETE_AXIOM = "DELETE_AXIOM";
    export const DEFINE_CONCEPT_SATISFIABILITY_CHECKING = "DEFINE_CONCEPT_SATISFIABILITY_CHECKING";
    export const DEFINE_INSTANCE_CHECKING = "DEFINE_INSTANCE_CHECKING";
    export const DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING = "DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING";

    export interface KnowledgeBaseAction extends AnyAction {
        knowledgeBaseId: number
    }

    export interface PAYLOAD_PARSE_AND_SAVE_EXPRESSION extends AnyAction {
        expressionString: string,
        vocabularyId: number,
        expressionCategory: ExpressionCategory
    }

    export interface PAYLOAD_CREATE_SYMBOL extends KnowledgeBaseAction {
        alphabet: VocabularyAlphabet,
        symbol: string
    }

    export interface PAYLOAD_RENAME_SYMBOL extends AnyAction {
        vocabularySymbolId: number,
        newSymbol: string
    }

    export interface PAYLOAD_DELETE_SYMBOL extends KnowledgeBaseAction {
        vocabularySymbolId: number
    }

    export interface PAYLOAD_SAVE_AXIOM extends KnowledgeBaseAction {
        axiomBox: keyof AxiomBoxPropertiesType,
        axiom: string,
        axiomId: number | undefined
    }

    export interface PAYLOAD_DELETE_AXIOM extends KnowledgeBaseAction {
        axiomId: number
    }

    export interface PAYLOAD_DEFINE_CONCEPT_SATISFIABILITY_CHECKING extends KnowledgeBaseAction, WorkspaceAction {
        conceptString: string
    }


    export interface PAYLOAD_DEFINE_INSTANCE_CHECKING extends KnowledgeBaseAction, WorkspaceAction {
        individualString: string
        conceptString: string
    }

    export interface PAYLOAD_DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING extends KnowledgeBaseAction, WorkspaceAction {}

    export function CREATE_PARSE_AND_SAVE_EXPRESSION (expressionString: string, vocabularyId: number, expressionCategory: ExpressionCategory): PAYLOAD_PARSE_AND_SAVE_EXPRESSION {
        return {
            type: PARSE_AND_SAVE_EXPRESSION,
            vocabularyId,
            expressionCategory,
            expressionString
        };
    }

    export function CREATE_SAVE_AXIOM (knowledgeBaseId: number, axiomBox: keyof AxiomBoxPropertiesType,
        axiom: string, axiomId: number | undefined): PAYLOAD_SAVE_AXIOM {
        return {
            type: SAVE_AXIOM,
            knowledgeBaseId,
            axiomBox,
            axiom,
            axiomId
        };
    }

    export function CREATE_DELETE_AXIOM (knowledgeBaseId: number, axiomId: number): PAYLOAD_DELETE_AXIOM {
        return {
            type: DELETE_AXIOM,
            knowledgeBaseId,
            axiomId
        };
    }

    export function CREATE_DEFINE_CONCEPT_SATISFIABILITY_CHECKING (conceptString: string, knowledgeBaseId: number, workspaceId: number): PAYLOAD_DEFINE_CONCEPT_SATISFIABILITY_CHECKING {
        return {
            type: DEFINE_CONCEPT_SATISFIABILITY_CHECKING,
            knowledgeBaseId,
            conceptString,
            workspaceId
        };
    }

    export function CREATE_DEFINE_INSTANCE_CHECKING (individualString: string, conceptString: string, knowledgeBaseId: number, workspaceId: number): PAYLOAD_DEFINE_INSTANCE_CHECKING {
        return {
            type: DEFINE_INSTANCE_CHECKING,
            knowledgeBaseId,
            individualString,
            conceptString,
            workspaceId
        };
    }

    export function CREATE_DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING (knowledgeBaseId: number, workspaceId: number): PAYLOAD_DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING {
        return {
            type: DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING,
            knowledgeBaseId,
            workspaceId
        };
    }
}

export default KNOWLEDGE_BASE_ACTIONS;