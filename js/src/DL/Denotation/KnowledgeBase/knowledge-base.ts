import "./Expression/expression-import-list";
import {createNewEmptyVocabulary, default as Vocabulary} from "../Vocabulary/vocabulary";
import {appendNormalizedToState} from "../../../Common/utils/util";
import {ExpressionCategory} from "./Expression/expression";
import {DLDenotationState} from "../denotation";

export default interface KnowledgeBase {
    id: number,
    vocabularyId: number,
    tBoxAxiomIds: number[],
    aBoxAxiomIds: number[],
    rBoxAxiomIds: number[]
}

export function createNewEmptyKnowledgeBase<S extends DLDenotationState> (state: S) {
    let vocabulary;
    [state, vocabulary] = createNewEmptyVocabulary(state) as [S, Vocabulary];

    const result = {
        id: NaN,
        vocabularyId: vocabulary.id,
        tBoxAxiomIds: [],
        aBoxAxiomIds: [],
        rBoxAxiomIds: []
    } as KnowledgeBase;

    state = appendNormalizedToState(state, ["DLKnowledgeBases"], result);

    return [state, result] as [S, KnowledgeBase];
}

export type AxiomBoxProperty = {
    AXIOM_CATEGORY: ExpressionCategory,
    KNOWLEDGE_BASE_KEY: string,
    LABEL: string
}

export type AxiomBoxPropertiesType = {
    [index: string]: AxiomBoxProperty,
    ABOX: AxiomBoxProperty,
    TBOX: AxiomBoxProperty,
    RBOX: AxiomBoxProperty
}

export const AxiomBoxProperties: AxiomBoxPropertiesType = {
    ABOX: {
        AXIOM_CATEGORY: ExpressionCategory.ABOX_AXIOM,
        KNOWLEDGE_BASE_KEY: "aBoxAxiomIds",
        LABEL: "ABox"
    },
    TBOX: {
        AXIOM_CATEGORY: ExpressionCategory.TBOX_AXIOM,
        KNOWLEDGE_BASE_KEY: "tBoxAxiomIds",
        LABEL: "TBox"
    },
    RBOX: {
        AXIOM_CATEGORY: ExpressionCategory.RBOX_AXIOM,
        KNOWLEDGE_BASE_KEY: "rBoxAxiomIds",
        LABEL: "RBox"
    }
};