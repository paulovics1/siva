import * as React from "react";
import {MouseEvent} from "react";
import {ListGroupItem} from "react-bootstrap";
import Latex from "react-latex";
import {VocabularySymbol} from "../vocabulary-symbol";
import autobind from "autobind-decorator";
import {MainComponent} from "../../../../App/components/main-component";
import RenameVocabularySymbolModal from "./rename-vocabulary-symbol-modal";
import {mapNormalizedStateIdCategory, NormalizedStateIdCategory} from "../../../../Common/utils/util";
import MainState from "../../../../App/flux/main-state";
import {Dispatch} from "redux";
import ConfirmationModal from "../../../../Common/components/confirmation-modal";
import VOCABULARY_ACTIONS from "../flux/vocabulary-actions";

export interface VocabularyColumnViewProps {
    vocabularyId: number
    vocabularySymbol: VocabularySymbol
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    symbolType: string,
    symbolToLatex: (item: any) => string
    fillAndShowModal: typeof MainComponent.prototype.fillAndShowModal
    editable: boolean
    nameFormatHint: string
    nameFormatConvert: (name: string) => string
    dispatch: Dispatch<MainState>
}

export default class VocabularySymbolView extends React.PureComponent<VocabularyColumnViewProps> {
    @autobind
    handleClick () {
        if (this.props.editable) {
            this.props.fillAndShowModal(RenameVocabularySymbolModal, {
                type: this.props.symbolType,
                originalName: this.props.vocabularySymbol.symbol,
                takenNames: new Set(mapNormalizedStateIdCategory(this.props.vocabularySymbols, vocabularySymbol => vocabularySymbol.symbol)),
                submitHandler: (name: string) => {
                    this.props.dispatch(VOCABULARY_ACTIONS.CREATE_RENAME_SYMBOL(this.props.vocabularySymbol.id, name));
                },
                formatHint: this.props.nameFormatHint,
                formatConvert: this.props.nameFormatConvert
            });
        }
    }

    @autobind
    handleRemoveClick (e: MouseEvent<HTMLElement>) {
        e.stopPropagation();

        this.props.fillAndShowModal(ConfirmationModal, {
            title: "Vocabulary symbol deletion confirmation",
            message: `Are you sure you want to delete "${this.props.vocabularySymbol.symbol}" vocabulary symbol?`,
            onConfirm: () => {
                this.props.dispatch(VOCABULARY_ACTIONS.CREATE_DELETE_SYMBOL(this.props.vocabularySymbol.id, this.props.vocabularyId));
            }
        });
    }

    render () {
        return (
            <ListGroupItem
                className="latex-ellipsis center"
                title={this.props.vocabularySymbol.symbol}
                onClick={this.props.editable ? this.handleClick : undefined}
            >
                <Latex
                    children={this.props.symbolToLatex(this.props.vocabularySymbol.symbol)}
                />
                {
                    !this.props.editable || <span
                        className="remove-icon glyphicon glyphicon-trash"
                        onClick={this.handleRemoveClick}
                        title={`Remove ${this.props.vocabularySymbol.symbol}`}
                    />
                }
            </ListGroupItem>
        );
    }
}