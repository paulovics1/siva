import {AnyAction} from "../../../../App/flux/actions";
import {VocabularyAlphabet} from "../vocabulary";

export namespace VOCABULARY_ACTIONS {
    export const CREATE_SYMBOL = "CREATE_SYMBOL";
    export const RENAME_SYMBOL = "RENAME_SYMBOL";
    export const DELETE_SYMBOL = "DELETE_SYMBOL";

    export interface VocabularyAction extends AnyAction {
        vocabularyId: number
    }

    export interface PAYLOAD_CREATE_SYMBOL extends VocabularyAction {
        alphabet: VocabularyAlphabet,
        symbol: string
    }

    export interface PAYLOAD_RENAME_SYMBOL extends AnyAction {
        vocabularySymbolId: number,
        newSymbol: string
    }

    export interface PAYLOAD_DELETE_SYMBOL extends VocabularyAction {
        vocabularySymbolId: number
    }

    export function CREATE_CREATE_SYMBOL (vocabularyId: number, alphabet: VocabularyAlphabet, symbol: string): PAYLOAD_CREATE_SYMBOL {
        return {
            type: CREATE_SYMBOL,
            vocabularyId,
            alphabet,
            symbol
        };
    }

    export function CREATE_RENAME_SYMBOL (vocabularySymbolId: number, newSymbol: string): PAYLOAD_RENAME_SYMBOL {
        return {
            type: RENAME_SYMBOL,
            vocabularySymbolId,
            newSymbol
        };
    }

    export function CREATE_DELETE_SYMBOL (vocabularySymbolId: number, vocabularyId: number): PAYLOAD_DELETE_SYMBOL {
        return {
            type: DELETE_SYMBOL,
            vocabularySymbolId,
            vocabularyId
        };
    }
}

export default VOCABULARY_ACTIONS;