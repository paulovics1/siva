export interface VocabularySymbol {
    id: number,
    symbol: string
}