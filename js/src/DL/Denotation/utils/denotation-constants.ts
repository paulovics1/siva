export namespace DLDenotationConstants {
    export const TAB_CONTAINER = {
        CLASS: {
            BASE: "dl-denotation"
        }
    };
    export const KNOWLEDGE_BASE = {
        TAB: {
            NAME: "Knowledge base",
            KEY: "KNOWLEDGE_BASE"
        }
    };
    export const VOCABULARY = {
        TAB: {
            NAME: "Vocabulary",
            KEY: "VOCABULARY"
        }
    };
}