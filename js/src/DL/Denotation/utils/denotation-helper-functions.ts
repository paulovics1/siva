import {AxiomBoxProperties, AxiomBoxPropertiesType, default as KnowledgeBase} from "../KnowledgeBase/knowledge-base";
import {VocabularyAlphabet} from "../Vocabulary/vocabulary";
import {appendNormalizedToState, appendToStateArray, getNewNormalizedId, NormalizedStateIdCategory, setInState, unsetInState} from "../../../Common/utils/util";
import {VocabularySymbol} from "../Vocabulary/vocabulary-symbol";
import Problem from "../../../Problem/problem";
import ExpressionRouter, {ExpressionUpdate} from "../KnowledgeBase/Expression/utils/expression-router";
import Expression, {ExpressionCategory} from "../KnowledgeBase/Expression/expression";
import {ErrorExpressionHelper} from "../KnowledgeBase/Expression/error-expression";
import {default as Individual, IndividualHelper} from "../KnowledgeBase/Expression/individual";
import {DLDenotationState} from "../denotation";

const DL_DENOTATION_HELPER_FUNCTIONS = {
    getKnowledgeBaseSimpleStringMap (state: DLDenotationState, knowledgeBaseId: number): KnowledgeBaseStringMap {
        const knowledgeBase = state.DLKnowledgeBases.byId[knowledgeBaseId];
        const knowledgeBaseBoxes = knowledgeBase as any as {[index: string]: number[]};

        const result: KnowledgeBaseStringMap = {};
        for (const axiomBoxKey in AxiomBoxProperties) {
            result[axiomBoxKey] = {};
            for (const axiomId of knowledgeBaseBoxes[AxiomBoxProperties[axiomBoxKey].KNOWLEDGE_BASE_KEY]) {
                result[axiomBoxKey][axiomId] = ExpressionRouter.getSimpleString(axiomId, state.DLExpressions, state.DLVocabularySymbols);
            }
        }

        return result;
    },
    reevaluateKnowledgeBaseAxioms<S extends DLDenotationState> (state: S, knowledgeBaseId: number, knowledgeBaseStringMap: KnowledgeBaseStringMap): S {
        const knowledgeBase = state.DLKnowledgeBases.byId[knowledgeBaseId];
        const vocabulary = state.DLVocabularies.byId[knowledgeBase.vocabularyId];

        for (const axiomBoxKey in AxiomBoxProperties) {
            if (knowledgeBaseStringMap[axiomBoxKey]) {
                for (const axiomIdString of Object.keys(knowledgeBaseStringMap[axiomBoxKey])) {
                    const axiomId = Number.parseInt(axiomIdString);
                    const axiomString = knowledgeBaseStringMap[axiomBoxKey][axiomId];
                    const expressionUpdate = ExpressionRouter.parseString(axiomString, state.DLExpressions, state.DLVocabularySymbols, vocabulary, AxiomBoxProperties[axiomBoxKey].AXIOM_CATEGORY);
                    state = DL_DENOTATION_HELPER_FUNCTIONS.saveParsedAxiom(state, knowledgeBaseId, axiomBoxKey, expressionUpdate, axiomId);
                }
            }
        }

        return state;
    },
    checkExpressionForErrors (expressionId: number, expressions: NormalizedStateIdCategory<Expression>) {
        const expression = expressions.byId[expressionId];
        let errors: number[] = [];

        if (expression.type === ErrorExpressionHelper.EXPRESSION_TYPE) {
            errors = [expressionId];
        }
        else {
            errors = ExpressionRouter.getRelatedExpressionIds(expressionId, expressions).filter(id => expressions.byId[id].type === ErrorExpressionHelper.EXPRESSION_TYPE);
        }

        return errors;
    },
    checkKnowledgeBaseForErrors<S extends DLDenotationState> (knowledgeBase: KnowledgeBase, expressions: NormalizedStateIdCategory<Expression>) {
        const knowledgeBaseBoxes = knowledgeBase as any as {[index: string]: number[]};
        const expressionIdSet = new Set();

        for (const axiomBoxKey in AxiomBoxProperties) {
            for (const axiomId of knowledgeBaseBoxes[AxiomBoxProperties[axiomBoxKey].KNOWLEDGE_BASE_KEY]) {
                ExpressionRouter.getRelatedExpressionIds(axiomId, expressions).forEach(rId => expressionIdSet.add(rId));
            }
        }

        const expressionIds = Array.from(expressionIdSet);

        return expressionIds.filter(id => expressions.byId[id].type === ErrorExpressionHelper.EXPRESSION_TYPE);
    },
    knowledgeBaseSet<S extends DLDenotationState> (state: S, knowledgeBaseId: number, attribute: keyof KnowledgeBase, newValue: any) {
        return setInState(state, ["DLKnowledgeBases", "byId", knowledgeBaseId, attribute], newValue, true);
    },
    getVocabulary (state: DLDenotationState, knowledgeBaseId: number) {
        return state.DLVocabularies.byId[state.DLKnowledgeBases.byId[knowledgeBaseId].vocabularyId];
    },
    addVocabularySymbol<S extends DLDenotationState> (state: S, vocabularyId: number, vocabularyAlphabet: VocabularyAlphabet, symbol: string) {
        const vocabularySymbol = {
            id: getNewNormalizedId(state.DLVocabularySymbols),
            symbol: symbol
        } as VocabularySymbol;

        state = appendToStateArray(state, ["DLVocabularies", "byId", vocabularyId, vocabularyAlphabet], vocabularySymbol.id, false);
        state = appendNormalizedToState(state, ["DLVocabularySymbols"], vocabularySymbol);
        return state;
    },
    deleteKnowledgeBaseAxiom<S extends DLDenotationState> (state: S, knowledgeBaseId: number, axiomId: number) {
        for (const axiomBoxKey in AxiomBoxProperties) {
            state = unsetInState(state, ["DLKnowledgeBases", "byId", knowledgeBaseId, AxiomBoxProperties[axiomBoxKey].KNOWLEDGE_BASE_KEY, axiomId]);
        }
        return state;
    },
    saveParsedAxiom<S extends DLDenotationState> (state: S, knowledgeBaseId: number, axiomBox: keyof AxiomBoxPropertiesType, parsedAxiom: ExpressionUpdate, originalAxiomId?: number) {

        state = setInState(state, ["DLExpressions"], parsedAxiom.expressions, true);

        if (originalAxiomId !== parsedAxiom.id) {
            state = unsetInState(state, ["DLKnowledgeBases", "byId", knowledgeBaseId, AxiomBoxProperties[axiomBox].KNOWLEDGE_BASE_KEY, originalAxiomId]);
            state = appendToStateArray(state, ["DLKnowledgeBases", "byId", knowledgeBaseId, AxiomBoxProperties[axiomBox].KNOWLEDGE_BASE_KEY], parsedAxiom.id, false);
        }

        return state;
    },
    addProblem<S extends DLDenotationState> (state: S, problem: Problem) {
        return state;
    },
    parseAndSaveExpression<S extends DLDenotationState> (state: S, expressionString: string, vocabularyId: number, expressionCategory: ExpressionCategory) {
        const update = ExpressionRouter.parseString(expressionString, state.DLExpressions, state.DLVocabularySymbols, state.DLVocabularies.byId[vocabularyId], expressionCategory);
        state = setInState(state, ["DLExpressions"], update.expressions, true);
        return [update.id, state] as [number, S];
    },
    getExpressionIndividuals (expressionId: number, expressions: NormalizedStateIdCategory<Expression>) {
        return new Set(ExpressionRouter
            .getRelatedExpressionIds(expressionId, expressions)
            .filter(rId => expressions.byId[rId].type === IndividualHelper.EXPRESSION_TYPE)
            .map(rId => (expressions.byId[rId] as Individual).symbolId));
    },
    getKnowledgeBaseIndividuals (knowledgeBase: KnowledgeBase, expressions: NormalizedStateIdCategory<Expression>) {
        const knowledgeBaseBoxes = knowledgeBase as any as {[index: string]: number[]};
        let individualIds = new Set();

        for (const axiomBoxKey in AxiomBoxProperties) {
            for (const axiomId of knowledgeBaseBoxes[AxiomBoxProperties[axiomBoxKey].KNOWLEDGE_BASE_KEY]) {
                const relatedIndividuals = ExpressionRouter
                    .getRelatedExpressionIds(axiomId, expressions)
                    .filter(rId => expressions.byId[rId].type === IndividualHelper.EXPRESSION_TYPE)
                    .map(rId => (expressions.byId[rId] as Individual).symbolId);

                relatedIndividuals.forEach(rId => individualIds.add(rId));
            }
        }

        return individualIds;
    }
};

export interface KnowledgeBaseStringMap {
    [axiomBoxKey: string]: {
        [axiomId: number]: string
    }
}

export default DL_DENOTATION_HELPER_FUNCTIONS;