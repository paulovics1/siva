import * as React from "react";
import ConceptSatisfiabilityChecking from "../concept-satisfiability-checking";
import Latex from "react-latex";
import {DLCTreeSolvableProblemLatexDescription} from "../utils/dl-ctree-solvable-problem-router";
import ExpressionRouter from "../../Denotation/KnowledgeBase/Expression/utils/expression-router";

export default class ConceptSatisfiabilityCheckingLatexDescription extends DLCTreeSolvableProblemLatexDescription {
    render () {
        const problem = this.props.cTreeView.props.problem as ConceptSatisfiabilityChecking;
        const conceptLatex = ExpressionRouter.getLatexString(problem.conceptId, this.props.cTreeView.props.expressions, this.props.cTreeView.vocabularySymbols);

        return (
            <div>
                <span>Concept satisfiability checking of </span>
                <Latex children={`$${conceptLatex}$`}/>
                <span> w.r.t. </span>
                <Latex children={`$\\mathcal{K}$`}/>
            </div>
        );
    }
}