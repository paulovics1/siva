import * as React from "react";
import {SyntheticEvent} from "react";
import autobind from "autobind-decorator";
import {Button, FormControl, FormGroup, Modal} from "react-bootstrap";
import KnowledgeBase from "../../Denotation/KnowledgeBase/knowledge-base";
import {VocabularySymbol} from "../../Denotation/Vocabulary/vocabulary-symbol";
import Vocabulary from "../../Denotation/Vocabulary/vocabulary";
import DLProblemRouter from "../utils/dl-problem-router";
import {ModalComponentProps, ModalComponentState} from "../../../Common/modal-component";
import {NormalizedStateIdCategory} from "../../../Common/utils/util";
import Expression from "../../Denotation/KnowledgeBase/Expression/expression";

export interface DLProblemChoiceModalProps extends ModalComponentProps {
    knowledgeBase: KnowledgeBase
    expressions: NormalizedStateIdCategory<Expression>
    vocabularySymbols: NormalizedStateIdCategory<VocabularySymbol>
    vocabulary: Vocabulary,
    workspaceId: number
}

export interface DLProblemChoiceModalState extends ModalComponentState {
}

export default class DLProblemChoiceModal extends React.PureComponent<DLProblemChoiceModalProps, DLProblemChoiceModalState> {
    select: HTMLInputElement;

    constructor (props: DLProblemChoiceModalProps) {
        super(props);

        this.state = {
            open: true
        };
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    @autobind
    handleConfirm (e?: SyntheticEvent<any>) {
        !e || e.preventDefault();
        this.hide();
        DLProblemRouter.handleDLProblemDefinitionRequest(this.select.value, this.props);
    }

    render () {
        const dlProblemHelpers = DLProblemRouter.getAllHelpers();

        return (
            <Modal
                className="modal-auto-width"
                show={this.state.open}
                onHide={this.hide}
                onExited={this.removeModal}
            >
                <form onSubmit={this.handleConfirm}>
                    <Modal.Header closeButton>
                        <Modal.Title>Type of problem to solve</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <FormGroup controlId="formControlsSelectMultiple">
                            <FormControl
                                inputRef={(ref) => this.select = ref!}
                                componentClass="select"
                            >
                                {
                                    dlProblemHelpers.map((helper, index) => (
                                        <option
                                            key={index}
                                            value={helper.PROBLEM_TYPE}>
                                            {helper.PROBLEM_NAME}
                                        </option>
                                    ))
                                }
                            </FormControl>
                        </FormGroup>
                    </Modal.Body>
                    <Modal.Footer>
                        <Button
                            type="submit"
                        >
                            Confirm
                        </Button>
                    </Modal.Footer>
                </form>
            </Modal>
        );
    }
}