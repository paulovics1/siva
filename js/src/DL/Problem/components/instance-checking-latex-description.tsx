import * as React from "react";
import Latex from "react-latex";
import {DLCTreeSolvableProblemLatexDescription} from "../utils/dl-ctree-solvable-problem-router";
import InstanceChecking from "../instance-checking";
import {individualSymbolToLatex} from "../../Denotation/Vocabulary/components/vocabulary-view";
import ExpressionRouter from "../../Denotation/KnowledgeBase/Expression/utils/expression-router";

export default class InstanceCheckingLatexDescription extends DLCTreeSolvableProblemLatexDescription {
    render () {
        const problem = this.props.cTreeView.props.problem as InstanceChecking;
        const individualLatex = individualSymbolToLatex(this.props.cTreeView.vocabularySymbols.byId[problem.individualSymbolId].symbol);
        const conceptLatex = ExpressionRouter.getLatexString(problem.conceptId, this.props.cTreeView.props.expressions, this.props.cTreeView.vocabularySymbols);

        return (
            <div>
                <span>Instance checking of individual </span>
                <Latex children={`$${individualLatex}$`}/>
                <span> and concept </span>
                <Latex children={`$${conceptLatex}$`}/>
                <span> w.r.t. </span>
                <Latex children={`$\\mathcal{K}$`}/>
            </div>
        );
    }
}