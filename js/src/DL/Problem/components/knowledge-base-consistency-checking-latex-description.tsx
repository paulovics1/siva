import * as React from "react";
import Latex from "react-latex";
import {DLCTreeSolvableProblemLatexDescription} from "../utils/dl-ctree-solvable-problem-router";

export default class KnowledgeBaseConsistencyCheckingLatexDescription extends DLCTreeSolvableProblemLatexDescription {
    render () {
        return (
            <div>
                <span>Knowledge base consistency checking of </span>
                <Latex children={`$\\mathcal{K}$`}/>
            </div>
        );
    }
}