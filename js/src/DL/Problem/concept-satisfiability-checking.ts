import {default as DLProblemRouter, DLKnowledgeBaseProblem} from "./utils/dl-problem-router";
import {DLProblemChoiceModalProps} from "./components/dl-problem-choice-modal";
import ConceptInputModal from "./components/concept-input-modal";
import DLCTreeSolvableProblemRouter from "./utils/dl-ctree-solvable-problem-router";
import ProblemRouter from "../../Problem/utils/problem-router";
import {filterNormalizedStateIdCategory, findInNormalizedStateIdCategory} from "../../Common/utils/util";
import ConceptSatisfiabilityCheckingLatexDescription from "./components/concept-satisfiability-checking-latex-description";
import ConceptRouter from "../Denotation/KnowledgeBase/Expression/utils/concept-router";
import ExpressionRouter from "../Denotation/KnowledgeBase/Expression/utils/expression-router";
import {CTreeState} from "../CTreeTableauReasoning/CompletionTree/c-tree";
import {CTREE_NODE_ORIGIN} from "../CTreeTableauReasoning/CompletionTree/Node/c-tree-node";
import CTreeView from "../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import CTreeViewMode from "../CTreeTableauReasoning/CompletionTree/Mode/components/c-tree-view-mode";
import CTREE_HELPER_FUNCTIONS from "../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import ModeSingleNodeInitialization from "../CTreeTableauReasoning/CompletionTree/Mode/components/mode-single-node-initialization";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";
import CTREE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/flux/c-tree-actions";
import KNOWLEDGE_BASE_ACTIONS from "../Denotation/KnowledgeBase/flux/knowledge-base-actions";

export default interface ConceptSatisfiabilityChecking extends DLKnowledgeBaseProblem {
    conceptId: number
}

export class ConceptSatisfiabilityCheckingHelper {
    static readonly PROBLEM_TYPE = "CONCEPT_SATISFIABILITY_CHECKING";
    static readonly PROBLEM_NAME = "Concept satisfiability checking";
    static readonly PROBLEM_DESCRIPTION = "Description";
    static readonly CTREE_PROBLEM_DESCRIPTION = ConceptSatisfiabilityCheckingLatexDescription;

    static create (conceptId: number, knowledgeBaseId: number): ConceptSatisfiabilityChecking {
        return {
            id: NaN,
            type: this.PROBLEM_TYPE,
            knowledgeBaseId,
            conceptId
        };
    }

    static handleDLProblemDefinitionRequest (dlProblemChoiceModalProps: DLProblemChoiceModalProps): void {
        dlProblemChoiceModalProps.fillAndShowModal(ConceptInputModal, {
            title: this.PROBLEM_NAME,
            onSubmit: (expressionString: string) => {
                dlProblemChoiceModalProps.dispatch(KNOWLEDGE_BASE_ACTIONS.CREATE_DEFINE_CONCEPT_SATISFIABILITY_CHECKING(expressionString, dlProblemChoiceModalProps.knowledgeBase.id, dlProblemChoiceModalProps.workspaceId));
            },
            expressions: dlProblemChoiceModalProps.expressions,
            vocabularySymbols: dlProblemChoiceModalProps.vocabularySymbols,
            vocabulary: dlProblemChoiceModalProps.vocabulary
        });
    }

    static isCTreeProblemInitialized (cTreeId: number, state: CTreeState) {
        const cTree = state.DLCTrees.byId[cTreeId];
        const filteredCTreeNodes = filterNormalizedStateIdCategory(state.DLCTreeNodes, cTree.nodes);
        const problem = state.Problems.byId[cTree.problemId];
        const nnfUpdate = ConceptRouter.getNNFExpressionUpdate((problem as ConceptSatisfiabilityChecking).conceptId, state.DLExpressions);

        const initialNode = findInNormalizedStateIdCategory(filteredCTreeNodes, node => node.origin === CTREE_NODE_ORIGIN.PROBLEM_INITIALIZATION && node.labelConcepts.indexOf(nnfUpdate.id) === 0);

        return initialNode !== null;
    }

    static getCTreeProblemInitializationMode (cTreeView: CTreeView): CTreeViewMode | undefined {
        const nodeName = CTREE_HELPER_FUNCTIONS.getNewNodeDefaultName(cTreeView.props.cTree, cTreeView.cTreeNodes, cTreeView.vocabularySymbols);
        const conceptSimpleString = ExpressionRouter.getSimpleString((cTreeView.props.problem as ConceptSatisfiabilityChecking).conceptId, cTreeView.props.expressions, cTreeView.vocabularySymbols);

        return new ModeSingleNodeInitialization(cTreeView, {
            nodeName: `${nodeName} (instance of ${conceptSimpleString})`,
            onNodeCreate: (x, y) => {
                cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_INITIALIZE_CONCEPT_SATISFIABILITY_CHECKING(x, y, cTreeView.props.problem.id, cTreeView.id));
            },
            onStartCallback: () => {
                cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_SET_STATUS(cTreeView.id, `[Problem initialization] Click to place a new completion tree node, whose label will contain ${conceptSimpleString}.`, "neutral"));
            }
        });
    }
}

ProblemRouter.registerHelper(ConceptSatisfiabilityCheckingHelper);
DLProblemRouter.registerHelper(ConceptSatisfiabilityCheckingHelper);
DLCTreeSolvableProblemRouter.registerHelper(ConceptSatisfiabilityCheckingHelper);