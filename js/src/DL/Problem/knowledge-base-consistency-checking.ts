import {DLProblemChoiceModalProps} from "./components/dl-problem-choice-modal";
import {default as DLProblemRouter, DLKnowledgeBaseProblem} from "./utils/dl-problem-router";
import ProblemRouter from "../../Problem/utils/problem-router";
import DLCTreeSolvableProblemRouter from "./utils/dl-ctree-solvable-problem-router";
import KnowledgeBaseConsistencyCheckingLatexDescription from "./components/knowledge-base-consistency-checking-latex-description";
import {CTreeState} from "../CTreeTableauReasoning/CompletionTree/c-tree";
import CTreeView from "../CTreeTableauReasoning/CompletionTree/components/c-tree-view";
import CTreeViewMode from "../CTreeTableauReasoning/CompletionTree/Mode/components/c-tree-view-mode";
import CTREE_HELPER_FUNCTIONS from "../CTreeTableauReasoning/CompletionTree/utils/c-tree-helper-functions";
import ModeSingleNodeInitialization from "../CTreeTableauReasoning/CompletionTree/Mode/components/mode-single-node-initialization";
import DL_CTREE_TABLEAU_WORKSPACE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/DLCTreeTableauWorkspace/flux/dl-ctree-tableau-workspace-actions";
import CTREE_ACTIONS from "../CTreeTableauReasoning/CompletionTree/flux/c-tree-actions";
import KNOWLEDGE_BASE_ACTIONS from "../Denotation/KnowledgeBase/flux/knowledge-base-actions";
import ModeKnowledgeBaseInitialization from "../CTreeTableauReasoning/CompletionTree/Mode/components/mode-knowledge-base-initialization";

export default interface KnowledgeBaseConsistencyChecking extends DLKnowledgeBaseProblem {

}

export class KnowledgeBaseConsistencyCheckingHelper {
    static readonly PROBLEM_TYPE = "KNOWLEDGE_BASE_CONSISTENCY_CHECKING";
    static readonly PROBLEM_NAME = "Knowledge base consistency checking";
    static readonly PROBLEM_DESCRIPTION = "Description";
    static readonly CTREE_PROBLEM_DESCRIPTION = KnowledgeBaseConsistencyCheckingLatexDescription;

    static create (knowledgeBaseId: number): KnowledgeBaseConsistencyChecking {
        return {
            id: NaN,
            type: this.PROBLEM_TYPE,
            knowledgeBaseId
        };
    }

    static handleDLProblemDefinitionRequest (dlProblemChoiceModalProps: DLProblemChoiceModalProps): void {
        dlProblemChoiceModalProps.dispatch(KNOWLEDGE_BASE_ACTIONS.CREATE_DEFINE_KNOWLEDGE_BASE_CONSISTENCY_CHECKING(dlProblemChoiceModalProps.knowledgeBase.id, dlProblemChoiceModalProps.workspaceId));
    }

    static isCTreeProblemInitialized (cTreeId: number, state: CTreeState) {
        const cTree = state.DLCTrees.byId[cTreeId];
        const knowledgeBase = state.DLKnowledgeBases.byId[cTree.knowledgeBaseId];

        return cTree.nodes.length > 0 || knowledgeBase.aBoxAxiomIds.length > 0;
    }

    static getCTreeProblemInitializationMode (cTreeView: CTreeView): CTreeViewMode | undefined {
        const nodeName = CTREE_HELPER_FUNCTIONS.getNewNodeDefaultName(cTreeView.props.cTree, cTreeView.cTreeNodes, cTreeView.vocabularySymbols);
        if (CTREE_HELPER_FUNCTIONS.getIndividualsToInitializeInCTree(cTreeView.props.knowledgeBase, cTreeView.props.expressions, cTreeView.cTreeNodes).length === 0) {
            return new ModeSingleNodeInitialization(cTreeView, {
                nodeName: `${nodeName} (instance of nnf(\u03a6), for every TBox axiom \u03a6)`,
                onNodeCreate: (x, y) => {
                    cTreeView.props.dispatch(DL_CTREE_TABLEAU_WORKSPACE_ACTIONS.CREATE_INITIALIZE_EMPTY_ABOX_KNOWLEDGE_BASE_CONSISTENCY_CHECKING(x, y, cTreeView.props.problem.id, cTreeView.id));
                },
                onStartCallback: () => {
                    cTreeView.props.dispatch(CTREE_ACTIONS.CREATE_SET_STATUS(cTreeView.id, "[Problem initialization] Click to place a new completion tree node, whose label will contain a NNF form of every axiom in the TBox.", "neutral"));
                }
            });
        }

        return new ModeKnowledgeBaseInitialization(cTreeView);
    }
}

ProblemRouter.registerHelper(KnowledgeBaseConsistencyCheckingHelper);
DLProblemRouter.registerHelper(KnowledgeBaseConsistencyCheckingHelper);
DLCTreeSolvableProblemRouter.registerHelper(KnowledgeBaseConsistencyCheckingHelper);