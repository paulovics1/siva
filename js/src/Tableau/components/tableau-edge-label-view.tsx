import * as React from "react";
import TableauView from "./tableau-view";
import TableauRuleRouter from "../utils/tableau-rule-router";
import TableauNode from "../tableau-node";
import {Dispatch} from "redux";
import MainState from "../../App/flux/main-state";
import {getAllRegexMatches} from "../../Common/utils/util";

type TableauEdgeLabelViewProps = {
    tableauNode: TableauNode,
    x1: number
    y1: number
    x2: number
    y2: number
    dispatch: Dispatch<MainState>
    leadsToTheCurrentState: boolean
    measureSVGText: typeof TableauView.prototype.measureSVGText
    tableauNodeDescriptionState: any
}

type TableauEdgeLabelViewState = {}

export default class TableauEdgeLabelView extends React.Component<TableauEdgeLabelViewProps, TableauEdgeLabelViewState> {
    constructor (props: TableauEdgeLabelViewProps) {
        super(props);
    }

    render () {
        const regex = /<b>(.*?)<\/b>|(.+?)(?=<b>|$)/gi;

        const text = TableauRuleRouter.getTableauNodeLabelText(this.props.tableauNode, this.props.tableauNodeDescriptionState);
        const textLines = text.split("\n").map(line => getAllRegexMatches(line, regex));
        const textHeight = this.props.measureSVGText("A").height;

        const textRotate = 180 + Math.atan2(this.props.y1 - this.props.y2, this.props.x1 - this.props.x2) / Math.PI * 180;

        return (
            <g
                transform={`translate(${(this.props.x1 + this.props.x2) / 2} ${(this.props.y1 + this.props.y2) / 2}) rotate(${textRotate})`}
            >
                {
                    textLines.map((line, index) => (
                        <text
                            textAnchor="middle"
                            dy={-(textLines.length - index) * textHeight}
                        >
                            {line.map(regexMatch => (
                                <tspan style={{
                                    fontWeight: regexMatch[1] ? "bold" : "normal"
                                }}>
                                    {regexMatch[1] ? regexMatch[1] : regexMatch[2]}
                                </tspan>
                            ))}
                        </text>
                    ))
                }
            </g>
        );
    }
}