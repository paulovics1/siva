import * as React from "react";
import TableauView from "./tableau-view";
import TableauEdgeLabelView from "./tableau-edge-label-view";
import TableauNode from "../tableau-node";
import {Dispatch} from "redux";
import MainState from "../../App/flux/main-state";

type TableauEdgeViewProps = {
    tableauNode: TableauNode,
    x1: number
    y1: number
    x2: number
    y2: number
    dispatch: Dispatch<MainState>
    leadsToTheCurrentState: boolean
    measureSVGText: typeof TableauView.prototype.measureSVGText
    tableauNodeDescriptionState: any
}

type TableauEdgeViewState = {}

export default class TableauEdgeView extends React.Component<TableauEdgeViewProps, TableauEdgeViewState> {
    constructor (props: TableauEdgeViewProps) {
        super(props);
    }

    render () {
        let labelCoords;

        if (this.props.x1 > this.props.x2) {
            labelCoords = {
                x1: this.props.x2,
                y1: this.props.y2,
                x2: this.props.x1,
                y2: this.props.y1
            };
        }
        else {
            labelCoords = {
                x1: this.props.x1,
                y1: this.props.y1,
                x2: this.props.x2,
                y2: this.props.y2
            };
        }
        return (
            <g
            >
                <TableauEdgeLabelView
                    {...labelCoords}
                    tableauNode={this.props.tableauNode}
                    tableauNodeDescriptionState={this.props.tableauNodeDescriptionState}
                    dispatch={this.props.dispatch}
                    leadsToTheCurrentState={this.props.leadsToTheCurrentState}
                    measureSVGText={this.props.measureSVGText}/>
                <path
                    stroke={this.props.leadsToTheCurrentState ? "#505050" : "#303030"}
                    strokeWidth={3}
                    d={`M ${this.props.x1} ${this.props.y1} L ${this.props.x2} ${this.props.y2}`}
                    filter={this.props.leadsToTheCurrentState ? "url(#active-shadow)" : ""}
                />
            </g>
        );
    }
}