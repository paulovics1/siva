import * as React from "react";
import {Modal} from "react-bootstrap";
import autobind from "autobind-decorator";
import TableauView from "./tableau-view";
import {Tableau} from "../tableau";
import TableauNode from "../tableau-node";
import {ModalComponentProps, ModalComponentState} from "../../Common/modal-component";
import {NormalizedStateIdCategory} from "../../Common/utils/util";

export interface TableauModalProps extends ModalComponentProps {
    tableau: Tableau
    tableauNodes: NormalizedStateIdCategory<TableauNode>
    tableauNodeDescriptionState: any
}

export interface TableauModalState extends ModalComponentState {
}

export default class TableauModal extends React.PureComponent<TableauModalProps, TableauModalState> {
    constructor (props: TableauModalProps) {
        super(props);

        this.state = {
            open: true,
            title: ""
        } as TableauModalState;
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    render () {
        return (
            <Modal
                className="modal-fullscreen tableau-modal"
                show={this.state.open}
                bsSize="large"
                onHide={this.hide}
                onExited={this.removeModal}
            >
                <Modal.Header closeButton>
                    <Modal.Title style={{textAlign: "center"}}>Tableau</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <TableauView
                        ref="a"
                        tableau={this.props.tableau}
                        tableauNodes={this.props.tableauNodes}
                        tableauNodeDescriptionState={this.props.tableauNodeDescriptionState}
                        dispatch={this.props.dispatch}
                    />
                </Modal.Body>
            </Modal>
        );
    }
}