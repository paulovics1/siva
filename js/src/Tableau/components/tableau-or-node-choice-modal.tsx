import * as React from "react";
import {Button, ButtonGroup, Modal} from "react-bootstrap";
import autobind from "autobind-decorator";
import {MainComponent} from "../../App/components/main-component";
import {ModalComponentProps, ModalComponentState} from "../../Common/modal-component";

export interface TableauOrNodeChoiceModalOption {
    index: number
    text: string
    removeModal: typeof MainComponent.prototype.removeModal
    disabled: boolean
}

export interface TableauOrNodeChoiceModalProps extends ModalComponentProps {
    title: string
    options: TableauOrNodeChoiceModalOption[]
    chooseHandler: (option: TableauOrNodeChoiceModalOption) => void
}

export interface TableauOrNodeChoiceModalState extends ModalComponentState {
}

export default class TableauOrNodeChoiceModal extends React.PureComponent<TableauOrNodeChoiceModalProps, TableauOrNodeChoiceModalState> {
    constructor (props: TableauOrNodeChoiceModalProps) {
        super(props);

        this.state = {
            open: true
        };
    }

    @autobind
    hide () {
        this.setState({
            open: false
        });
    }

    choose (option: TableauOrNodeChoiceModalOption) {
        this.props.chooseHandler(option);
        this.hide();
    }

    @autobind
    removeModal () {
        this.props.removeModal(this.props.id);
    }

    render () {
        return (
            <Modal
                show={this.state.open}
                onHide={this.hide}
                onExited={this.removeModal}
            >
                <Modal.Header closeButton>
                    <Modal.Title>{this.props.title}</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <ButtonGroup
                        className="flex--even"
                    >
                        {
                            this.props.options.map((option, index) => (
                                <Button
                                    key={index}
                                    bsSize="large"
                                    disabled={option.disabled}
                                    onClick={() => {this.choose(option);}}
                                >
                                    {option.text}
                                </Button>
                            ))
                        }
                    </ButtonGroup>
                </Modal.Body>
            </Modal>
        );
    }
}