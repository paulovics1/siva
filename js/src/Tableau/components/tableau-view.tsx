import * as React from "react";
import {ReactNode} from "react";
import {findInNormalizedStateIdCategory, getNormalizedStateIdCategoryIterator, mapNormalizedStateIdCategory, Measure2D, measureSVGText, NormalizedStateIdCategory, Point2D, updateBoundingBox} from "../../Common/utils/util";
import TableauNode from "../tableau-node";
import {Tableau} from "../tableau";
import {TableauConstants} from "../utils/tableau-constants";
import TableauNodeView from "./tableau-node-view";
import TableauEdgeView from "./tableau-edge-view";
import autobind from "autobind-decorator";
import {Dispatch} from "redux";
import MainState from "../../App/flux/main-state";

export interface TableauViewProps {
    tableau: Tableau
    tableauNodes: NormalizedStateIdCategory<TableauNode>
    tableauNodeDescriptionState: any
    dispatch: Dispatch<MainState>
}

export interface TableauViewState {
    wrapperWidth: number
    wrapperHeight: number
}

export default class TableauView extends React.PureComponent<TableauViewProps, TableauViewState> {
    refs: {
        measureTextNode: SVGTextElement
        svgContainer: HTMLDivElement
    };

    constructor (props: TableauViewProps) {
        super(props);

        this.state = {
            wrapperWidth: 0,
            wrapperHeight: 0
        };
    }

    getTableauNodesCoords () {
        const result: {[index: number]: Point2D} = {};
        const widths: {[index: number]: number} = {};
        const parents: {[index: number]: number} = {};
        const rows: {[index: number]: number[]} = {};

        const nodeQueue = [[this.props.tableau.rootNodeId, 0]];

        while (nodeQueue.length > 0) {
            const [nodeId, row] = nodeQueue.shift()!;
            const node = this.props.tableauNodes.byId[nodeId];

            if (typeof rows[row] === "undefined") {
                rows[row] = [];
            }

            rows[row].push(nodeId);

            for (const childId of node.childrenIds) {
                parents[childId] = nodeId;
                nodeQueue.push([childId, row + 1]);
            }
        }

        for (let rowNum = Object.keys(rows).length - 1; rowNum >= 0; rowNum--) {
            for (const nodeId of rows[rowNum]) {
                const node = this.props.tableauNodes.byId[nodeId];

                if (node.childrenIds.length === 0) {
                    widths[nodeId] = TableauConstants.NODE.SPACING.X;
                }
                else {
                    widths[nodeId] = 0;

                    for (const childId of node.childrenIds) {
                        widths[nodeId] += widths[childId];
                    }
                }
            }

            let maxWidth = rows[rowNum].reduce((max, nodeId) => Math.max(max, widths[nodeId]), 0);

            for (const nodeId of rows[rowNum]) {
                widths[nodeId] = maxWidth;
            }
        }

        const coordsQueue = [[this.props.tableau.rootNodeId, 0, 0]];

        while (coordsQueue.length > 0) {
            const [nodeId, x, y] = coordsQueue.shift()!;
            const node = this.props.tableauNodes.byId[nodeId];

            result[nodeId] = {x, y};

            const childrenWidth = node.childrenIds.reduce((acc, childId) => acc + widths[childId], 0);

            let leftOffset = x - childrenWidth / 2;

            for (const childId of node.childrenIds) {
                coordsQueue.push([childId, leftOffset + widths[childId] / 2, y + TableauConstants.NODE.SPACING.Y]);
                leftOffset += widths[childId];
            }
        }

        return result;
    }

    getNodesLeadingToCurrentState () {
        const result = [this.props.tableau.activeNodeId];

        while (result[result.length - 1] !== this.props.tableau.rootNodeId) {
            result.push(findInNormalizedStateIdCategory(this.props.tableauNodes, node => node.childrenIds.indexOf(result[result.length - 1]) !== -1)!.id);
        }

        return result;
    }


    componentDidMount (): void {
        this.setState({
            wrapperWidth: this.refs.svgContainer.clientWidth,
            wrapperHeight: this.refs.svgContainer.clientHeight
        });
    }

    @autobind
    measureSVGText (text: string): Measure2D {
        return measureSVGText(text, this.refs.measureTextNode);
    }


    render () {
        const nodesCoords = this.getTableauNodesCoords();
        const nodesLeadingToCurrentState = this.getNodesLeadingToCurrentState();

        const rootNodeCoords = nodesCoords[this.props.tableau.rootNodeId];

        const padding = 40;
        const boundingBox = {
            left: rootNodeCoords.x - padding,
            top: rootNodeCoords.y - padding,
            right: rootNodeCoords.x + padding,
            bottom: rootNodeCoords.y + padding
        };

        const center = {
            x: rootNodeCoords.x,
            y: rootNodeCoords.y
        };

        const tableauNodes = mapNormalizedStateIdCategory(this.props.tableauNodes, node => {
            updateBoundingBox(boundingBox, nodesCoords[node.id], padding);

            return (
                <TableauNodeView
                    key={node.id}
                    tableauNode={node}
                    tableau={this.props.tableau}
                    x={nodesCoords[node.id].x}
                    y={nodesCoords[node.id].y}
                    dispatch={this.props.dispatch}
                    leadsToTheCurrentState={nodesLeadingToCurrentState.indexOf(node.id) !== -1}
                />
            );
        });

        const tableauEdges: ReactNode[] = [];

        for (const node of getNormalizedStateIdCategoryIterator(this.props.tableauNodes)) {
            node.childrenIds.reduce((acc, childId) => {
                acc.push(
                    <TableauEdgeView
                        x1={nodesCoords[node.id].x}
                        y1={nodesCoords[node.id].y}
                        x2={nodesCoords[childId].x}
                        y2={nodesCoords[childId].y}
                        tableauNode={this.props.tableauNodes.byId[childId]}
                        tableauNodeDescriptionState={this.props.tableauNodeDescriptionState}
                        dispatch={this.props.dispatch}
                        measureSVGText={this.measureSVGText}
                        leadsToTheCurrentState={nodesLeadingToCurrentState.indexOf(childId) !== -1}/>
                );
                return acc;
            }, tableauEdges);
        }

        if (boundingBox.bottom - boundingBox.top < this.state.wrapperHeight) {
            boundingBox.bottom = this.state.wrapperHeight + boundingBox.top;
        }

        if (boundingBox.right - boundingBox.left < this.state.wrapperWidth) {
            boundingBox.right = this.state.wrapperWidth / 2;
            boundingBox.left = -this.state.wrapperWidth / 2;
        }

        const centerBoundingBox = {
            left: center.x - this.state.wrapperWidth / 2,
            top: center.y - this.state.wrapperHeight / 2,
            right: center.x + this.state.wrapperWidth / 2,
            bottom: center.y + this.state.wrapperHeight / 2
        };

        return (
            <div
                ref={"svgContainer"}
                className="svg-container"
            >
                <div
                    className="svg-wrapper"
                >
                    <svg
                        shapeRendering="geometricPrecision"
                        textRendering="geometricPrecision"
                        viewBox={`${boundingBox.left} ${boundingBox.top} ${boundingBox.right - boundingBox.left} ${boundingBox.bottom - boundingBox.top}`}
                        width={boundingBox.right - boundingBox.left}
                        height={boundingBox.bottom - boundingBox.top}
                    >
                        <defs>
                            <filter id="active-shadow"
                                    filterUnits="userSpaceOnUse"
                                    x="-100%"
                                    y="-100%"
                                    height="200%"
                                    width="200%"
                            >
                                <feGaussianBlur result="blurOut" in="SourceAlpha" stdDeviation="3"/>
                                <feColorMatrix in="blurOut" result="matrixOut" type="matrix"
                                               values="0 0 0 0 0.3   0 0 0 0 0.3  0 0 0 0 1   0 0 0 1 0"/>
                                <feComponentTransfer in="matrixOut" result="transferOut">
                                    <feFuncA type="linear" slope="6"/>
                                </feComponentTransfer>
                                <feBlend in="SourceGraphic" in2="transferOut" mode="darken"/>
                            </filter>
                        </defs>
                        {
                            tableauEdges
                        }
                        {
                            tableauNodes
                        }
                        <text
                            ref="measureTextNode"
                            visibility="hidden"
                            pointerEvents="none"
                        />
                    </svg>
                </div>
            </div>
        );
    }
}