import {TableauState} from "./tableau";
import {appendNormalizedToState} from "../Common/utils/util";

export default interface TableauNode {
    id: number
    actionType: string
    childrenIds: number[]
    state: TableauNodeState | undefined
}

export function createEmptyRootTableauNode<S extends TableauState> (state: S) {
    const result = {
        id: NaN,
        actionType: "ROOT",
        childrenIds: [],
        state: undefined
    } as TableauNode;

    state = appendNormalizedToState(state, ["TableauNodes"], result);

    return [state, result] as [S, TableauNode];
}

export enum TableauNodeState {
    NEUTRAL,
    FAILURE,
    SUCCESS
}