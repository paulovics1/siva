import TableauNode, {createEmptyRootTableauNode} from "./tableau-node";
import {appendNormalizedToState, NormalizedStateIdCategory} from "../Common/utils/util";

export interface Tableau {
    id: number
    rootNodeId: number
    activeNodeId: number
    type: string
}

export function createNewEmptyTableau<S extends TableauState> (state: S, tableauType: string) {
    let rootNode;
    [state, rootNode] = createEmptyRootTableauNode(state);

    const result = {
        id: NaN,
        rootNodeId: rootNode.id,
        activeNodeId: rootNode.id,
        type: tableauType
    } as Tableau;

    state = appendNormalizedToState(state, ["Tableaus"], result);

    return [state, result] as [S, Tableau];
}

export interface TableauState {
    Tableaus: NormalizedStateIdCategory<Tableau>,
    TableauNodes: NormalizedStateIdCategory<TableauNode>
}