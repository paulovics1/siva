import {filterNormalizedStateIdCategory, NormalizedStateIdCategory} from "../../Common/utils/util";
import TableauNode from "../tableau-node";
import {Tableau} from "../tableau";

const TABLEAU_HELPER_FUNCTIONS = {
    getTableauNodes (rootNodeId: number, tableauNodes: NormalizedStateIdCategory<TableauNode>): NormalizedStateIdCategory<TableauNode> {
        function getSubtreeNodes (rootNodeId: number, tableauNodes: NormalizedStateIdCategory<TableauNode>) {
            let result = [rootNodeId];
            const currentNode = tableauNodes.byId[rootNodeId];

            for (const childId of currentNode.childrenIds) {
                result.push(...getSubtreeNodes(childId, tableauNodes));
            }

            return result;
        }

        return filterNormalizedStateIdCategory(tableauNodes, getSubtreeNodes(rootNodeId, tableauNodes));
    },
    getNodeParent (tableauNodeId: number, tableau: Tableau, tableauNodes: NormalizedStateIdCategory<TableauNode>) {
        const queue = [tableau.rootNodeId];

        while (queue.length > 0) {
            const nodeId = queue.pop()!;
            const node = tableauNodes.byId[nodeId];

            for (const childId of node.childrenIds) {
                if (childId === tableauNodeId) {
                    return node;
                }
                queue.push(childId);
            }
        }

        return null;
    },
    getNodesInfoMap (rootNodeId: number, tableauNodes: NormalizedStateIdCategory<TableauNode>) {
        const nodesInfo: {[nodeId: number]: {parentId: number, level: number}} = {};

        nodesInfo[rootNodeId] = {
            parentId: NaN,
            level: 0
        };

        const queue = [[rootNodeId, 0]];

        while (queue.length > 0) {
            const poppedNode = queue.pop()!;
            const node = tableauNodes.byId[poppedNode[0]];

            for (const childId of node.childrenIds) {
                nodesInfo[childId] = {parentId: poppedNode[0], level: poppedNode[1] + 1};
                queue.push([childId, poppedNode[1] + 1]);
            }
        }

        return nodesInfo;
    }
};

export default TABLEAU_HELPER_FUNCTIONS;