import {Reducer} from "redux";
import MainState from "../../App/flux/main-state";
import {default as Workspace, WorkspaceProps, WorkspaceView} from "../workspace";
import {ClassNameRegister} from "../../Common/utils/util";

class WorkspaceRegisterClass extends ClassNameRegister<WorkspaceClass> {
}

export interface WorkspaceClass<P extends WorkspaceProps = WorkspaceProps> {
    new (props: P, context?: any): WorkspaceView,

    factory (state: MainState, ...args: any[]): [MainState, Workspace],

    reducer: Reducer<MainState>

    workspaceLabel: string
}

const WorkspaceRegister = new WorkspaceRegisterClass();

export default WorkspaceRegister;