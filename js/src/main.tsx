import * as React from "react";
import * as ReactDOM from "react-dom";
import {Provider} from "react-redux";
import Main from "./App/components/main-component";
import store from "./App/flux/store";
import "../../css/src/main.scss";

ReactDOM.render(
    <Provider store={store}>
        <Main/>
    </Provider>,
    document.querySelector('#react-mount')
);