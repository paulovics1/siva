const ExtractTextPlugin = require("extract-text-webpack-plugin");
const webpack = require("webpack");

module.exports = {
    entry: {
        main: "./js/src/main.tsx"
    },
    output: {
        path: __dirname + "/js/build",
        filename: "[name].js",
        chunkFilename: "[name].js"
    },
    resolve: {
        extensions: [".ts", ".tsx", ".js", ".json"]
    },
    module: {
        exprContextCritical: false,
        rules: [
            {
                test: /\.tsx?$/,
                loaders: ['awesome-typescript-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.(jsx?)$/,
                loaders: ['babel-loader', 'source-map-loader'],
                exclude: [/node_modules/]
            },
            {
                test: /\.less/,
                loaders: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'less-loader']
                })
            },
            {
                test: /\.scss/,
                loaders: ExtractTextPlugin.extract({
                    fallback: 'style-loader',
                    use: ['css-loader', 'postcss-loader', 'sass-loader']
                })
            }
        ]
    },
    node: {
        fs: 'empty',
        child_process: 'empty'
    },
    externals: {
        "react": "React",
        "react-dom": "ReactDOM",
        'redux': 'Redux',
        'react-redux': 'ReactRedux',
        "katex": "katex"
    },
    plugins: [
        new ExtractTextPlugin({
            filename: "../../css/build/[name].css",
            allChunks: true
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "vendor.js",
            minChunks (module, count) {
                var context = module.context;
                return context && context.indexOf('node_modules') >= 0;
            }
        })
    ]
};